//
//  AddingTasksTVCell.swift
//  GoPatoHome
//
//  Created by 3Embed on 22/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit


/// This is AddingTasksTVCell Configuration, it will take TextBasicModel as input type and give you a customize collectionView Cell according to your input
typealias addingTasksTVCellConfig = TableCellConfigurator<AddingTasksTVCell , TextBasicModel>



/// AddingTasksTVCellDelegate , it will help you to add tasks in a purticular section ,
protocol AddingTasksTVCellDelegate : class {
    /// pressedAddTaskButton , it is AddingTasksTVCellDelegate method. that provide you AddingTasksTVCell in parameter
    ///- Parameters:
    /// (cell : AddingTasksTVCell )
    func pressedAddTaskButton(cell : AddingTasksTVCell)
}

class AddingTasksTVCell: UITableViewCell , ConfigurableCell {

    @IBOutlet weak var addTasksView: MoreServicesView!
    @IBOutlet weak var addTaskButton: UIButton!
    weak var delegate : AddingTasksTVCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .white 
        addTasksView.lblServiceDescription.isHidden = true
        addTasksView.leftImage.isHidden = true
        addTasksView.leftImageView.isHidden = true
        addTaskButton.isHidden = true
        addTasksView.rightViewButton.isHidden = false 
        addTasksView.rightViewButton.addTarget(self, action: #selector(pressedAddTaskButton(sender:)), for: .touchUpInside)
       // self.addTaskButton.addTarget(self, action: #selector(pressedAddTaskButton(sender:)), for: .touchUpInside)
    }

    
    func configure(data: TextBasicModel) {
        addTasksView.tf_AddingTasks.placeholder =  data.getTextBasicModelElements().6
         addTasksView.tf_AddingTasks.isHidden = false 
        addTasksView.lblServiceName.isHidden = ( data.getTextBasicModelElements().0 == "" ? true : false )
        addTasksView.rightViewButton.tag = Int(data.getTextBasicModelElements().7)
        addTasksView.rightImage.image = data.getTextBasicModelElements().5
    }
    
    
    @objc func pressedAddTaskButton(sender : UIButton){
        delegate?.pressedAddTaskButton(cell: self)
    }
  
    
}
