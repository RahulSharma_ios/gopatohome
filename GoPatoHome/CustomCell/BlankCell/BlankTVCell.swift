//
//  BlankTVCell.swift
//  GoPatoHome
//
//  Created by 3Embed on 20/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

/// This is BlankTVCell Configuration, it will take (UIColor, String , UIColor ) as input type and give you a customize tableView Cell according to your input
/// also used for adding blank space or blank cell in tableview

typealias  blankTVCellConfig = TableCellConfigurator<BlankTVCell , (UIColor, String , UIColor)>

typealias  blankTVCellConfig1 = TableCellConfigurator<BlankTVCell , (UIColor, String , UIColor)>

class BlankTVCell: UITableViewCell , ConfigurableCell {

    @IBOutlet weak var lbl_Text: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lbl_Text.text = ""
    }

    
    func configure(data: (UIColor, String , UIColor )) {
        
        self.contentView.backgroundColor = data.0
        self.lbl_Text.textColor = data.2
        self.lbl_Text.text = data.1
    
    }
    
    
    
}
