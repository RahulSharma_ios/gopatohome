//
//  CalanderCVCell.swift
//  GoPatoHome
//
//  Created by 3Embed on 13/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

/// This is CalanderCVCell Configuration, it will take CalenderViewModel as input type and give you a customize collectionView Cell according to your input

///


typealias calanderCVCellConfigurator = CollectionCellConfigurator<CalanderCVCell, CalenderViewModel>

/// CalanderCVCellDelegate , this delegate helps you to select the value from the calander View
protocol CalanderCVCellDelegate : class  {
    /// selectDateForViewTasks , its delegate method that return the CalanderCVCell as parameter in the function
    /// - Parameters:
    /// cell : CalanderCVCell
    func selectDateForViewTasks(cell : CalanderCVCell)
}


class CalanderCVCell: UICollectionViewCell  , ConfigurableCell{
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var selectedView: UIView!
    private var commom_Variables_and_Methods : Common_Variables_and_Methods!
    
    @IBOutlet weak var lblWeekDays: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var pharma: UIView!
    @IBOutlet weak var service: UIView!
    @IBOutlet weak var home: UIView!
    @IBOutlet weak var store: UIView!
    
    @IBOutlet weak var hiddenButton: UIButton!
    weak var delegate : CalanderCVCellDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.containerView.backgroundColor = Theme.shared.tableViewBackGround
        commom_Variables_and_Methods = Common_Variables_and_Methods.getInstance()
        setupView()
    }

    private func setupView(){
        
        pharma.layer.masksToBounds = true
        service.layer.masksToBounds = true
        home.layer.masksToBounds = true
        store.layer.masksToBounds = true
        
        pharma.layer.cornerRadius = pharma.frame.height
        service.layer.cornerRadius = service.frame.height
        home.layer.cornerRadius = home.frame.height
        store.layer.cornerRadius = store.frame.height

    }
    
    func configure(data: CalenderViewModel){
        
        home.isHidden = data.getBoolArray()[0]
        store.isHidden = data.getBoolArray()[1]
        pharma.isHidden = data.getBoolArray()[2]
        service.isHidden = data.getBoolArray()[3]
        
        self.selectedView.backgroundColor =  ( !data.getShodow() ?  Theme.shared.tableViewBackGround : .white )
        
        if data.getShodow(){
            selectedView.addShadow(offset: CGSize(width: 0, height: 0), color: Theme.shared.customGrayColor, opacity: 0.7, radius: 3.0)
            selectedView.layer.cornerRadius = 10
            lbl_Date.font = UIFont.boldSystemFont(ofSize: 27)   
            
        }else{
             lbl_Date.font = UIFont.systemFont(ofSize: 21)
             selectedView.addShadow(offset: CGSize(width: 0, height: 0), color: Theme.shared.tableViewBackGround, opacity: 0.0, radius: 0.0)
        }
       
        hiddenButton.tag = data.getIndexValue()
    }
    
    @IBAction func pressedForSelectDate(_ sender: UIButton) {
        delegate?.selectDateForViewTasks(cell: self)
    }
    
    
    deinit {
        print("CalanderCVCell deinit")
    }
}
