//
//  CalenderHolderTVCell.swift
//  GoPatoHome
//
//  Created by 3Embed on 19/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

/// This is CalenderHolderTVCell Configuration, it will take CalenderViewModel array as input type and give you a customize tableView Cell according to your input


typealias calenderHolderTVCell = TableCellConfigurator<CalenderHolderTVCell ,[CalenderViewModel] >

protocol  CalenderHolderTVCellDelegate : class  {
    func selectDateForViewTasks(cell : CalanderCVCell)
}

class CalenderHolderTVCell: UITableViewCell , ConfigurableCell{

    fileprivate var configurators: [CellConfigurator] = []
    @IBOutlet weak var collectionView: UICollectionView!
     weak var delegate : CalenderHolderTVCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.backgroundColor = Theme.shared.tableViewBackGround
        
        self.contentView.backgroundColor = Theme.shared.tableViewBackGround
        registerCells()
        setupView()
    }

    fileprivate func registerCells() {
        self.collectionView.register(UINib(nibName: "CalanderCVCell", bundle: nil), forCellWithReuseIdentifier: "CalanderCVCell")
    }


    func configure(data: [CalenderViewModel]) {
        createConfigurators(data: data)
    }


    fileprivate func createConfigurators(data :  [CalenderViewModel]){
        self.configurators.removeAll(keepingCapacity: false)
        
        for item in data{
            print(item.getShodow())
               self.configurators.append(calanderCVCellConfigurator(item: item))
        }
        
        self.collectionView.reloadData()
    }

    fileprivate func setupView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }

    
    deinit {
        print("CalenderHolderTVCell deinit")
    }
    
}




extension CalenderHolderTVCell : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return configurators.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = self.configurators[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        
        
        if cell is CalanderCVCell {
            (cell as! CalanderCVCell).delegate = self
        }

        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width / 8
            , height: 100)
    }
    
}


extension  CalenderHolderTVCell : CalanderCVCellDelegate {
    
    func selectDateForViewTasks(cell: CalanderCVCell) {
        delegate?.selectDateForViewTasks(cell: cell)
    }
    
    
    
}
