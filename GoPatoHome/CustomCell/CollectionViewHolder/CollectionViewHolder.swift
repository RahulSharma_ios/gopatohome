//
//  CollectionViewHolder.swift
//  GoPatoHome
//
//  Created by 3Embed on 16/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

/// This is CollectionViewHolder Configuration, it will take  CalenderViewModel array as input type and give you a customize tableView Cell according to your input
typealias collectionViewHolderTVCell = TableCellConfigurator<CollectionViewHolder ,[CalenderViewModel] >

/// This is custom UITableViewCell Class, it's containing a collection view, that help you display the tasks info.

class CollectionViewHolder: UITableViewCell , ConfigurableCell{

    @IBOutlet weak var collectionView: UICollectionView!
    /// it's CellConfigurator type array variable that contains all the configuration of the collection view
    fileprivate var configurators: [CellConfigurator] = []
  
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: .scrollCalanderToIndex, object: nil)
        // Initialization code
         self.contentView.backgroundColor = Theme.shared.tableViewBackGround
        registerCells()
        setupView()
    }

    
    fileprivate func registerCells() {
        self.collectionView.register(UINib(nibName: "TasksCVCell", bundle: nil), forCellWithReuseIdentifier: "TasksCVCell")
    }
    
    
    func configure(data: [CalenderViewModel]) {
        createConfigurators(data: data)
    }
    
    
    fileprivate func createConfigurators(data :  [CalenderViewModel]){
        self.configurators.removeAll(keepingCapacity: false)
        
        for item in data{
             self.configurators.append(tasksCVCellConfigurator(item: item))
        }
    }
    
    @objc func showSpinningWheel(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["rowData"] as? Int{
                
                let indexPath = IndexPath(item: id, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: [.centeredVertically,   .centeredHorizontally], animated: true)
                
            }
        }
    }
    
    fileprivate func setupView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
}



// MARK:- collection View delegate and datasource methods 
extension CollectionViewHolder : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return configurators.count
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = self.configurators[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        
         return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       return CGSize(width: self.frame.width 
        , height: 175)
    }
    
    
}
