//
//  CommentTVCell.swift
//  GoPatoHome
//
//  Created by 3Embed on 23/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

typealias commentTVCellConfig  = TableCellConfigurator<CommentTVCell , (String , UIColor , UIFont) >

class CommentTVCell: UITableViewCell , ConfigurableCell{

    @IBOutlet weak var commentView: CustomTextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .white
    }

    
    func configure(data: (String , UIColor , UIFont)) {
        commentView.setPlaceHolder(placeHolderText: data.0, placeHolderColor: data.1, placeHolderFont: data.2)
    }
    
    
    
}
