//
//  DatePickerTVCell.swift
//  GoPatoHome
//
//  Created by 3Embed on 20/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

/// This is DatePickerTVCell Configuration, it will take (String , String , UIColor , UIImage ) as input type and give you a customize tableView Cell according to your input


typealias datePickerTVCellConfig = TableCellConfigurator<DatePickerTVCell , (String , String , UIColor , UIImage)>


/// This is DatePickerTVCellDelegate , it contains a function

protocol  DatePickerTVCellDelegate  : class  {
    /// This is DatePickerTVCellDelegate method , that contains DatePickerTVCell as perameter
    ///- Parameters:
    ///(cell : DatePickerTVCell)
    
    func pressedDatePicker(cell : DatePickerTVCell)
}

class DatePickerTVCell: UITableViewCell , ConfigurableCell{

    @IBOutlet weak var outlineView: UIView!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var icon: UIImageView!
    
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var saperator: UIView!
    
    weak var delegate : DatePickerTVCellDelegate?
    
    private var common_Variables_and_Methods : Common_Variables_and_Methods!
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .white
        self.contentView.backgroundColor = .white
        common_Variables_and_Methods = Common_Variables_and_Methods.getInstance()
        common_Variables_and_Methods.setBorderAndcorners(view: outlineView)
        saperator.backgroundColor = Theme.shared.allowViewColor
        
    }
    
    func configure(data: (String , String ,UIColor , UIImage)) {
        
        lblDate.text = data.0
        lblTime.text = data.1
        
        lblDate.textColor = data.2
        lblTime.textColor = data.2
        icon.image = data.3
        
    }

    @IBAction func pressedHiddenButton(_ sender: UIButton) {
        delegate?.pressedDatePicker(cell: self )
    }
    
}
