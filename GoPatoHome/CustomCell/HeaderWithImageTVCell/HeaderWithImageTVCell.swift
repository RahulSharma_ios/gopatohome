//
//  HeaderWithImageTVCell.swift
//  GoPatoHome
//
//  Created by 3Embed on 11/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

/// This is HeaderWithImageTVCell Configuration, it will take HeaderWithImageModel as input type and give you a customize tableView Cell according to your input

typealias headerWithImageTVCellConfigurator = TableCellConfigurator<HeaderWithImageTVCell, HeaderWithImageModel>


protocol HeaderWithImageTVCellDelegate : class {
    func pressedbutton(cell : HeaderWithImageTVCell)
}

class HeaderWithImageTVCell: UITableViewCell , ConfigurableCell {

    @IBOutlet weak var nestyImage: UIButton!
    weak var delegate : HeaderWithImageTVCellDelegate?
    @IBOutlet weak var taskImageView: TaskImageView!
    @IBOutlet weak var lbl_Header: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func configure(data: HeaderWithImageModel){
        
//        if data.getHeaderElemets().4 {
//            lbl_Header.isHidden = true
//            lbl_Header.layer.opacity = 0.0
//
//          UIView.animate(withDuration: data.getHeaderElemets().5, delay: data.getHeaderElemets().6, options: [.transitionFlipFromBottom], animations: {
//                self.lbl_Header.layer.opacity += 1.0
//                self.lbl_Header.isHidden = false
//            }, completion: nil)
//            
//        }
        
        self.taskImageView.taskImageView.image =  data.getHeaderElemets().1
        self.taskImageView.isHidden = data.getHeaderElemets().0
        self.taskImageView.lbl_TasksCounter.text = data.getHeaderElemets().2
        self.taskImageView.lbl_TasksCounter.isHidden = (data.getHeaderElemets().2 == "" ? true : false )
        
        self.contentView.backgroundColor = data.getHeaderElemets().6
        nestyImage.isHidden = !data.getHeaderElemets().3
        nestyImage.setImage(data.getHeaderElemets().4, for: .normal)
        lbl_Header.text = data.getHeaderElemets().5
        lbl_Header.font = data.getHeaderElemets().12
        
        if data.getButtonTag() != 0 {
            nestyImage.tag = Int(data.getButtonTag())
            nestyImage.addTarget(self, action: #selector(pressedNestyImage(sender:)), for: .touchUpInside)
        }
       
    }
    
    @objc func pressedNestyImage(sender : UIButton){
        delegate?.pressedbutton(cell: self)
    }
    
    
    
}
