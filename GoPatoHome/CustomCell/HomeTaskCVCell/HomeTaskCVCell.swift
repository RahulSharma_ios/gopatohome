//
//  HomeTaskCVCell.swift
//  GoPatoHome
//
//  Created by 3Embed on 22/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

/// This is HomeTaskCVCell Configuration, it will take TextBasicModel as input type and give you a customize collectionView Cell according to your input

typealias homeTaskCVCellConfig  =  CollectionCellConfigurator<HomeTaskCVCell, TextBasicModel>

class HomeTaskCVCell: UICollectionViewCell , ConfigurableCell {
    @IBOutlet weak var obj_MoreServicesView: MoreServicesView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func configure(data: TextBasicModel) {
        
        obj_MoreServicesView.containerView.backgroundColor = .white
        
        obj_MoreServicesView.leftImageView.isHidden = data.getTextBasicModelElements().3
        
        obj_MoreServicesView.lblServiceDescription.isHidden = data.getTextBasicModelElements().4
        
        obj_MoreServicesView.lblServiceName.text = data.getTextBasicModelElements().0
        
        obj_MoreServicesView.lblServiceName.font = data.getTextBasicModelElements().1
        
        obj_MoreServicesView.rightImage.image = data.getTextBasicModelElements().5
        
        obj_MoreServicesView.tf_AddingTasks.isHidden =  ( data.getTextBasicModelElements().6 == "" ? true : false )
        
    }

}
