//
//  HomeTasksHolderTVCell.swift
//  GoPatoHome
//
//  Created by 3Embed on 22/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit
/// This is HomeTasksHolderTVCell Configuration, it will take String as input type and give you a customize tableView Cell according to your input

typealias  homeTasksHolderConfig = TableCellConfigurator<HomeTasksHolderTVCell , [TextBasicModel]>

class HomeTasksHolderTVCell: UITableViewCell , ConfigurableCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var configurators : [CellConfigurator] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .white
        self.registerCell()
        
        self.setupView()
    }

    
    private  func registerCell(){
        self.collectionView.register(UINib.init(nibName: "HomeTaskCVCell", bundle: nil), forCellWithReuseIdentifier: "HomeTaskCVCell")
    }
    
    fileprivate func setupView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func configure(data: [TextBasicModel]) {
        configurators.removeAll(keepingCapacity: false )
        
        for item in data{
         
            self.configurators.append(homeTaskCVCellConfig(item: item))
        }
        self.collectionView.reloadData()
        
    }

    deinit {
        print("HomeTasksHolderTVCell deinit")
    }
    
}


extension HomeTasksHolderTVCell : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return configurators.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = self.configurators[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        
        
        if cell is CalanderCVCell {
//            (cell as! CalanderCVCell).delegate = self
        }
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.frame.width ) / 2
            , height: 60)
    }
    
}


