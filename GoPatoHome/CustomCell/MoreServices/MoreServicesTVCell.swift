//
//  MoreServicesTVCell.swift
//  GoPatoHome
//
//  Created by 3Embed on 15/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit


typealias moreServicesTVCellConfig = TableCellConfigurator<MoreServicesTVCell , TextBasicModel>

class MoreServicesTVCell: UITableViewCell , ConfigurableCell{

    @IBOutlet weak var obj_MoreServicesView: MoreServicesView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
    func configure(data: TextBasicModel) {
        
        
//        TextBasicModel.init(tintColor: .white, bgColor: .white, textColor: .black, image: #imageLiteral(resourceName: "HappyFace"), text: "Pato Stores", subText: "Products", textFont: Theme.shared.taskNameTextFont, subTextFont: Theme.shared.subTextFont, subTextColor: .lightGray)
        
        obj_MoreServicesView.tf_AddingTasks.isHidden = true
        obj_MoreServicesView.rightImageView.isHidden = false
        obj_MoreServicesView.rightImage.image = data.getDashboardBasicModel().10
        obj_MoreServicesView.containerView.backgroundColor = data.getDashboardBasicModel().1
        
        obj_MoreServicesView.lblServiceName.textColor = data.getDashboardBasicModel().2
        
        obj_MoreServicesView.leftImage.image = data.getDashboardBasicModel().3
        
        obj_MoreServicesView.lblServiceName.text = data.getDashboardBasicModel().4
        
         obj_MoreServicesView.lblServiceDescription.text = data.getDashboardBasicModel().5
        
        obj_MoreServicesView.lblServiceName.font = data.getDashboardBasicModel().6
        
        obj_MoreServicesView.lblServiceDescription.font = data.getDashboardBasicModel().7
        obj_MoreServicesView.lblServiceDescription.textColor = data.getDashboardBasicModel().8
        obj_MoreServicesView.imageWidth.constant = data.getDashboardBasicModel().9
         obj_MoreServicesView.stackHeight.constant = data.getDashboardBasicModel().9
        
        
    }
    
    func dataSource(data : (taskName : String  , desc : String, image : UIImage ,playPauseImage : UIImage )){
        
        obj_MoreServicesView.lblServiceName.text = data.taskName
        obj_MoreServicesView.lblServiceDescription.text = data.desc
        obj_MoreServicesView.leftImage.image = data.image
        
        obj_MoreServicesView.rightImage.image = data.playPauseImage
    }

}
