//
//  TasksCVCell.swift
//  GoPatoHome
//
//  Created by 3Embed on 12/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

/// This is TasksCVCell Configuration, it will take CalenderViewModel as input type and give you a customize collectionView Cell according to your input
typealias tasksCVCellConfigurator = CollectionCellConfigurator<TasksCVCell,CalenderViewModel>


class TasksCVCell: UICollectionViewCell , ConfigurableCell{

    
    @IBOutlet weak var homeTaksView: TaskImageView!
    
    @IBOutlet weak var supermarketTasksView: TaskImageView!
    
    @IBOutlet weak var pharmecyTasksView: TaskImageView!
    
    @IBOutlet weak var Servicios: TaskImageView!
    
    @IBOutlet weak var lbl_Date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = Theme.shared.tableViewBackGround
      
//        homeTaksView.taskImageView.image = #imageLiteral(resourceName: "supermarket")
//        supermarketTasksView.taskImageView.image = #imageLiteral(resourceName: "supermarket")
//        pharmecyTasksView.taskImageView.image = #imageLiteral(resourceName: "pharma")
//        Servicios.taskImageView.image = #imageLiteral(resourceName: "serviceGray")
        
    }
    
    
    func configure(data: CalenderViewModel){
        
        
        homeTaksView.taskImageView.image = #imageLiteral(resourceName: "tareas")
        supermarketTasksView.taskImageView.image = #imageLiteral(resourceName: "supermarket")
        pharmecyTasksView.taskImageView.image = #imageLiteral(resourceName: "pharma")
        Servicios.taskImageView.image = #imageLiteral(resourceName: "serviceGray")
        
        lbl_Date.text = data.getDate()
        
        homeTaksView.lbl_TasksCounter.text = data.getTasksCounter()[0]
        supermarketTasksView.lbl_TasksCounter.text = data.getTasksCounter()[1]
        pharmecyTasksView.lbl_TasksCounter.text = data.getTasksCounter()[2]
        Servicios.lbl_TasksCounter.text = data.getTasksCounter()[3]
    
        
    }
    
    
    
    
    
    

}
