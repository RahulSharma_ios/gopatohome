//
//  BottomViewOfApplication.swift
//  GoPatoHome
//
//  Created by 3Embed on 11/04/19.
//  Copyright © 2019 GoPato. All rights reserved.


import UIKit

protocol BottomViewOfApplicationDelegate : class  {
    func pressedLisrButton()
    func pressedHomeButton()
    func pressedChatButton()
    func pressedAvatorButton()
}


class BottomViewOfApplication: UIView {

    
    weak var delegate : BottomViewOfApplicationDelegate?
    
    let tappedValue : CGFloat = -20.0
    let tappedLeftandRightValue : CGFloat = -15
    
    @IBOutlet var view: UIView!
    
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var buttonList: UIButton!
    
    @IBOutlet weak var buttonHome: UIButton!
    
    @IBOutlet weak var buttonChat: UIButton!
    
    @IBOutlet weak var buttonNest: UIButton!
    
    
    @IBOutlet weak var buttonHomeBackground: UIView!
    
    @IBOutlet weak var buttonNestBackground: UIView!
    @IBOutlet weak var buttonListBackground: UIView!
    
    @IBOutlet weak var buttonChatBackground: UIView!
    
    
    
    
    // home button constraints
    
    @IBOutlet weak var homeButtonTopAnchor: NSLayoutConstraint!
    @IBOutlet weak var homeButtonLeftAnchor: NSLayoutConstraint!
    @IBOutlet weak var homeButtonRightAnchor: NSLayoutConstraint!
    
    
    @IBOutlet weak var homeButtonBackgroundRightAnchor: NSLayoutConstraint!
    @IBOutlet weak var homeButtonBackgroundLeftAnchor: NSLayoutConstraint!
    @IBOutlet weak var homeButtonBackgroundTopAnchor: NSLayoutConstraint!
    
    
    
    
    // chat button constraints
    
    
    @IBOutlet weak var chatButtonTopAnchor: NSLayoutConstraint!
    @IBOutlet weak var chatButtonLeftAnchor: NSLayoutConstraint!
    @IBOutlet weak var chatButtonRightAnchor: NSLayoutConstraint!
    
    
    @IBOutlet weak var chatButtonBackgroundRightAnchor: NSLayoutConstraint!
    @IBOutlet weak var chatButtonBackgroundLeftAnchor: NSLayoutConstraint!
    @IBOutlet weak var chatButtonBackgroundTopAnchor: NSLayoutConstraint!
    
    
    
    // list button constraints
    
    
    @IBOutlet weak var listButtonTopAnchor: NSLayoutConstraint!
    @IBOutlet weak var listButtonLeftAnchor: NSLayoutConstraint!
    @IBOutlet weak var listButtonRightAnchor: NSLayoutConstraint!
    
    
    @IBOutlet weak var listButtonBackgroundRightAnchor: NSLayoutConstraint!
    @IBOutlet weak var listButtonBackgroundLeftAnchor: NSLayoutConstraint!
    @IBOutlet weak var listButtonBackgroundTopAnchor: NSLayoutConstraint!
    
    
    
    
    // avator button constraints
    @IBOutlet weak var avatorButtonTopAnchor: NSLayoutConstraint!
    @IBOutlet weak var avatorButtonLeftAnchor: NSLayoutConstraint!
    @IBOutlet weak var avatorButtonRightAnchor: NSLayoutConstraint!
    
    
    @IBOutlet weak var avatorButtonBackgroundRightAnchor: NSLayoutConstraint!
    @IBOutlet weak var avatorButtonBackgroundLeftAnchor: NSLayoutConstraint!
    @IBOutlet weak var avatorButtonBackgroundTopAnchor: NSLayoutConstraint!
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("BottomViewOfApplication", owner: self, options: nil)
          view.frame = self.bounds
        
        
        shadowView.addShadow(offset: CGSize(width: 0.0, height: 0), color: .lightGray, opacity: 0.7, radius: 4)
        
    
        shadowView.layer.cornerRadius = 10
        
        shadowView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.backgroundColor = .clear
        
        
        self.addSubview(view)
   
        self.setupButtons()
        buttonHome.accessibilityIdentifier = "1"
           self.setupView(anchorsArray: getHomeButtonConstraints(), button: buttonHome, backgroundView:  buttonHomeBackground)

    }
    
    private func setupButtons(){
        
        buttonList.accessibilityIdentifier = "0"
        buttonHome.accessibilityIdentifier = "0"
        buttonChat.accessibilityIdentifier = "0"
        buttonNest.accessibilityIdentifier = "0"
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    private func setupView( anchorsArray:  [NSLayoutConstraint] , button : UIButton , backgroundView : UIView ){

        anchorsArray[0].constant = tappedValue
        anchorsArray[1].constant = tappedLeftandRightValue
        anchorsArray[2].constant = tappedLeftandRightValue
        
        anchorsArray[3].constant = tappedValue
        anchorsArray[4].constant = tappedLeftandRightValue - 2
        anchorsArray[5].constant = tappedLeftandRightValue - 2
        
        button.layer.cornerRadius = button.frame.width / 2 + 16
        button.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        
        backgroundView.addShadow(offset: CGSize(width: 0.0, height: 0), color: .lightGray, opacity: 0.7, radius: 4)
        
        backgroundView.layer.cornerRadius = backgroundView.frame.width / 2 + 20
        backgroundView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
    }
    
    
    
    
    private func removeViewShadow( anchorsArray:  [NSLayoutConstraint] , button : UIButton , backgroundView : UIView ){
        anchorsArray[0].constant = 0
        anchorsArray[1].constant = 0
        anchorsArray[2].constant = 0
        
        anchorsArray[3].constant = 0
        anchorsArray[4].constant = 0
        anchorsArray[5].constant = 0
        
        button.layer.cornerRadius = 0
        button.layer.maskedCorners = []
        
        
        backgroundView.layer.shadowOpacity = 0.0
        backgroundView.layer.shadowColor = UIColor.lightGray.cgColor
        backgroundView.layer.shadowRadius = 0.0
        backgroundView.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        backgroundView.layer.cornerRadius = 0
        backgroundView.layer.maskedCorners = []
        
    }
    
    private func getHomeButtonConstraints() -> [NSLayoutConstraint]{
        
        let homeButtonAnchors : [NSLayoutConstraint] = [homeButtonTopAnchor,
                                                        homeButtonLeftAnchor,
                                                        homeButtonRightAnchor,
                                                        homeButtonBackgroundTopAnchor,
                                                        homeButtonBackgroundRightAnchor,
                                                        homeButtonBackgroundLeftAnchor]
        
        return homeButtonAnchors
    }
    
    
    
    private func getChatButtonConstraints() -> [NSLayoutConstraint]{
        let chatButtonAnchors : [NSLayoutConstraint] = [chatButtonTopAnchor,
                                                        chatButtonLeftAnchor,
                                                        chatButtonRightAnchor,
                                                        chatButtonBackgroundTopAnchor,
                                                        chatButtonBackgroundRightAnchor,
                                                        chatButtonBackgroundLeftAnchor]
        
        return chatButtonAnchors
    }
    
    
    
    private func getListButtonConstraints() -> [NSLayoutConstraint]{
        let listButtonAnchors : [NSLayoutConstraint] = [listButtonTopAnchor,
                                                        listButtonLeftAnchor,
                                                        listButtonRightAnchor,
                                                        listButtonBackgroundTopAnchor,
                                                        listButtonBackgroundRightAnchor,
                                                        listButtonBackgroundLeftAnchor]
        
        return listButtonAnchors
    }
    
    private func getAvatorButtonConstraints() -> [NSLayoutConstraint]{
        let avatorButtonAnchors : [NSLayoutConstraint] = [avatorButtonTopAnchor,
                                                          avatorButtonLeftAnchor,
                                                          avatorButtonRightAnchor,
                                                          avatorButtonBackgroundTopAnchor,
                                                          avatorButtonBackgroundRightAnchor,
                                                          avatorButtonBackgroundLeftAnchor]
        
        return avatorButtonAnchors
    }
    
    @IBAction func pressedListButton(_ sender: UIButton) {
      
    if buttonList.accessibilityIdentifier == "0"{
    
        self.removeViewShadow(anchorsArray: getHomeButtonConstraints() , button: buttonHome, backgroundView: buttonHomeBackground)
        
        self.removeViewShadow(anchorsArray: getChatButtonConstraints(), button: buttonChat, backgroundView: buttonChatBackground)
        
        self.removeViewShadow(anchorsArray: getAvatorButtonConstraints(), button: buttonNest, backgroundView: buttonNestBackground)
        
          self.setupView(anchorsArray: getListButtonConstraints(), button: buttonList, backgroundView: buttonListBackground)
            setupButtons()
            buttonList.accessibilityIdentifier = "1"
        
        delegate?.pressedLisrButton()
        }
    }
    
    
    
    @IBAction func pressedHomeButton(_ sender: UIButton) {
        
        if buttonHome.accessibilityIdentifier == "0"{
//            buttonHome.tintColor = .white
//            buttonHome.backgroundColor = Theme.shared.homeImageColor
//            buttonHome.layer.cornerRadius = buttonHome.frame.height / 2
            self.setupView(anchorsArray: getHomeButtonConstraints() , button: buttonHome, backgroundView: buttonHomeBackground)
            
            self.removeViewShadow(anchorsArray: getChatButtonConstraints(), button: buttonChat, backgroundView: buttonChatBackground)
            
            self.removeViewShadow(anchorsArray: getAvatorButtonConstraints(), button: buttonNest, backgroundView: buttonNestBackground)
            
            self.removeViewShadow(anchorsArray: getListButtonConstraints(), button: buttonList, backgroundView: buttonListBackground)
            
            self.setupButtons()
            buttonHome.accessibilityIdentifier = "1"
            delegate?.pressedHomeButton()
        }
        
        
        
    }
    
    @IBAction func pressedChatButton(_ sender: UIButton) {
        
        
        if buttonChat.accessibilityIdentifier == "0"{
        self.setupView(anchorsArray: getChatButtonConstraints(), button: buttonChat, backgroundView: buttonChatBackground)
        
        self.removeViewShadow(anchorsArray: getHomeButtonConstraints() , button: buttonHome, backgroundView: buttonHomeBackground)
        
       
        self.removeViewShadow(anchorsArray: getAvatorButtonConstraints(), button: buttonNest, backgroundView: buttonNestBackground)
        
        self.removeViewShadow(anchorsArray: getListButtonConstraints(), button: buttonList, backgroundView: buttonListBackground)
        
        self.setupButtons()
        buttonChat.accessibilityIdentifier = "1"
            delegate?.pressedChatButton()
        }
    }
    
    
    @IBAction func pressedAvatarButton(_ sender: UIButton) {
        
        if buttonNest.accessibilityIdentifier == "0"{
            
        self.setupView(anchorsArray: getAvatorButtonConstraints(), button: buttonNest, backgroundView: buttonNestBackground)
        
        self.removeViewShadow(anchorsArray: getHomeButtonConstraints() , button: buttonHome, backgroundView: buttonHomeBackground)
        
        self.removeViewShadow(anchorsArray: getChatButtonConstraints(), button: buttonChat, backgroundView: buttonChatBackground)
        
       
        self.removeViewShadow(anchorsArray: getListButtonConstraints(), button: buttonList, backgroundView: buttonListBackground)
            setupButtons()
            buttonNest.accessibilityIdentifier = "1"
            delegate?.pressedAvatorButton()
        }
    }
    
}
