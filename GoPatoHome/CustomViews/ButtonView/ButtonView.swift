//
//  ButtonView.swift
//  GoPatoHome
//
//  Created by 3Embed on 17/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

protocol  ButtonViewDelegate : class  {
    func pressedBlueButton()
    func pressedRedButton()
}

class ButtonView: UIView {

    weak var delegate : ButtonViewDelegate?
    
    private var common_Variables_and_Methods : Common_Variables_and_Methods!
    @IBOutlet var view: UIView!
    
    @IBOutlet weak var leftAnchor_View: NSLayoutConstraint!
    
    @IBOutlet weak var rightAnchor_View: NSLayoutConstraint!
    
    @IBOutlet weak var redView: UIView!
    @IBOutlet weak var blueView: UIView!
    
    @IBOutlet weak var redViewTitleLbl: UILabel!
    
    @IBOutlet weak var redCross: UIButton!
    @IBOutlet weak var blueTick: UIButton!
    @IBOutlet weak var blueViewLeftIcon: UIView!
    
    @IBOutlet weak var blueViewWidth: NSLayoutConstraint!
    @IBOutlet weak var blueViewButton: UIButton!
    
    @IBOutlet weak var blueViewTitleLbl: UILabel!
    @IBOutlet weak var redViewButton: UIButton!
    
    @IBOutlet weak var redViewEclips: UIImageView!
    
    @IBOutlet weak var blueViewRightIcon: UIView!
    @IBOutlet weak var redViewHeight: NSLayoutConstraint!
    
    
    
    @IBOutlet weak var blueViewHeight: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commitInit()
    }

    
    
    private func commitInit(){
        self.common_Variables_and_Methods = Common_Variables_and_Methods.getInstance()
        
        Bundle.main.loadNibNamed("ButtonView", owner: self, options: nil)
        
        view.fixInView(self)
        
        
        leftAnchor_View.constant = -(self.frame.width - 200)
        rightAnchor_View.constant = 0
        
        blueView.addShadow(offset: CGSize(width: 0.0, height: 0.0), color: .lightGray, opacity:0.7 , radius: 4.0)
        
        blueView.layer.cornerRadius = 25
        
        
        redViewHeight.constant = 40
        
        redView.addShadow(offset: CGSize(width: 0.0, height: 0.0), color: .lightGray, opacity:0.7 , radius: 4.0)
        
        redView.layer.cornerRadius = 20
       
        self.redView.backgroundColor = .white
        self.blueView.backgroundColor = .getUIColor(color: "4C82EE")
        
        blueViewButton.addTarget(self, action: #selector(pressedBlueViewButton), for: .touchUpInside)
        
        self.view.transform = CGAffineTransform(translationX: -20, y: 0)
        
        redViewButton.addTarget(self, action: #selector(pressedRedViewButton), for: .touchUpInside)
        redViewButton.accessibilityIdentifier = "0"
        blueViewButton.accessibilityIdentifier = "1"
        
        
        
    }
    
    
    @objc func pressedRedViewButton(){
        
        if redViewButton.accessibilityIdentifier == "0"{
            self.redViewButton.accessibilityIdentifier = "1"
            self.blueViewButton.accessibilityIdentifier = "0"
        UIView.animate(withDuration: 0.3) {
            self.view.transform = CGAffineTransform(translationX: 190, y: 0)
            
                self.redView.backgroundColor = .getUIColor(color: "F95C5C")
                self.blueView.backgroundColor = .white
                self.blueView.layer.cornerRadius -= 5
                self.redView.layer.cornerRadius += 5
                 self.redViewHeight.constant = 50
                 self.blueViewHeight.constant = 40
            
                 self.redViewButton.addShadow(offset: CGSize(width: 0.0, height: 0.0), color: .lightGray, opacity:0.7 , radius: 10.0)
                  self.redViewButton.clipsToBounds = false
            self.redCross.tintColor = .white
            self.blueViewLeftIcon.isHidden = false
            self.blueTick.tintColor = .lightGray
//            self.blueViewTitleLbl.isHidden = true
            }
            
        }else{
            delegate?.pressedRedButton()
        }
        
        print(self.frame.width - 200)
        
    }
    
    @objc func pressedBlueViewButton(){
         self.blueViewButton.addShadow(offset: CGSize(width: 0.0, height: 0.0), color: .lightGray, opacity:0.7 , radius: 10.0)
        
        if self.blueViewButton.accessibilityIdentifier == "0"{
        
            UIView.animate(withDuration: 0.3) {
                
                
                self.blueViewButton.clipsToBounds = false
                self.view.transform = CGAffineTransform(translationX: -20, y: 0)
                self.redView.backgroundColor = .white
                self.blueView.backgroundColor = .getUIColor(color: "4C82EE")
                
                self.blueView.layer.cornerRadius += 5
                self.redView.layer.cornerRadius -= 5
                self.redViewHeight.constant = 40
                self.blueViewHeight.constant = 50
                self.blueViewLeftIcon.isHidden = true
                self.redCross.tintColor = .lightGray
            }
            self.redViewButton.accessibilityIdentifier = "0"
            self.blueViewButton.accessibilityIdentifier = "1"
        }else{
              delegate?.pressedBlueButton()
        }
        print(self.frame.width - 200)
    }
    
    
}


