//
//  CustomCalanderView.swift
//  GoPatoHome
//
//  Created by 3Embed on 23/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

class CustomCalanderView: UIView {
    @IBOutlet var containerView: UIView!
    private var common_Variables_and_Methods : Common_Variables_and_Methods!
    @IBOutlet weak var datePickerView: UIPickerView!
    
    @IBOutlet weak var monthPickerView: UIPickerView!
    private var isMovable : Bool = false
    private var days : Int!
    private var DateArray = [Int]()
    
    let monthsArray = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    
    @IBOutlet weak var saperatorView: UIView!
    @IBOutlet weak var headerView: UIView!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitView()
    }

    
    override init(frame: CGRect) {
        super.init(frame:frame)
        commitView()
    }
    
    
    private func commitView(){
        Bundle.main.loadNibNamed("CustomCalanderView", owner: self, options: nil)
        containerView.fixInView(self)
        common_Variables_and_Methods = Common_Variables_and_Methods.getInstance()
       
        
        setupView()
        self.containerView.layer.cornerRadius = 15.0
        self.headerView.layer.cornerRadius = 15.0
        self.headerView.backgroundColor = .white
        self.containerView.addShadow(offset: CGSize(width: 0.0, height: 6.0), color: .lightGray, opacity: 0.7, radius: 6.0)
        
      self.saperatorView.backgroundColor = Theme.shared.allowViewColor
        
    }
    
    
    private func setupView(){
        
        datePickerView.setValue(Theme.shared.allowViewColor, forKey: "textColor")
        datePickerView.delegate = self
        datePickerView.delegate = self
        
        monthPickerView.delegate = self
        monthPickerView.dataSource = self
        
        
    }
}


extension Date {
    var currentDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd,MM,YYYY"
        return dateFormatter.string(from: self)
    }
    
     func lastDay(ofMonth m: Int, year y: Int) -> Int {
        let cal = Calendar.current
        var comps = DateComponents(calendar: cal, year: y, month: m)
        comps.setValue(m + 1, for: .month)
        comps.setValue(0, for: .day)
        let date = cal.date(from: comps)!
        return cal.component(.day, from: date)
    }
    
}



extension CustomCalanderView : UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let date = Date()
        let m = date.currentDate.split(separator: ",")
        days = date.lastDay(ofMonth: Int("\(m[1])")!, year: Int("\(m[2])")!)
        
        DateArray = [ Int("\(m[0])")! , Int("\(m[1])")! , Int("\(m[2])")!]
        
        if pickerView == datePickerView{
               return days
        } else if pickerView == monthPickerView {
               return monthsArray.count
        }
        return 0
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
        if pickerView == datePickerView{
          
                return "\(row+1)"
            
        }else if pickerView == monthPickerView{
                return "\(monthsArray[row].prefix(3))"
        }
        return "0"
        
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
//    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
//
//        let label : UILabel?
//
//        if view == nil{
//            label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: pickerView.frame.width / 3, height:  pickerView.frame.height))
//        }else {
//            label = view as? UILabel
//        }
//        label?.text = "\(row+1)"
//        label?.textAlignment = .center
//        label?.textColor = .black
//        label?.layer.cornerRadius = (label?.frame.height)!  / 2
//        label?.layer.borderColor = Theme.shared.allowViewColor.cgColor
//        label?.layer.borderWidth = 1.0
//
//        return label!
//    }
  
    
    
}
