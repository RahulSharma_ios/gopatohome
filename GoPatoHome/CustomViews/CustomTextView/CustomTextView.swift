//
//  CustomTextView.swift
//  GoPatoHome
//
//  Created by 3Embed on 23/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

class CustomTextView: UIView {

    @IBOutlet var containerView: UIView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    
    private var placeHolderText : String!
    private var placeHolderColor : UIColor!
    private var placeHolderFont : UIFont!
    
    private var common_Variables_and_Methods : Common_Variables_and_Methods!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitView()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commitView()
    }

    private func commitView(){
        Bundle.main.loadNibNamed("CustomTextView", owner: self, options: nil)
        containerView.fixInView(self)
        common_Variables_and_Methods = Common_Variables_and_Methods.getInstance()
        
        self.containerView.layer.cornerRadius = 15.0
        self.containerView.addShadow(offset: CGSize(width: 0.0, height: 0.0), color: .lightGray, opacity: 0.7, radius: 2.0)
        
        textView.delegate = self
        textView.returnKeyType = .done
        
    }
    
 /// function used to set placeholder in textview
    func setPlaceHolder(placeHolderText : String , placeHolderColor : UIColor , placeHolderFont : UIFont ){
        do {
            textView.text = placeHolderText
            textView.textColor = placeHolderColor
            textView.font = placeHolderFont
            
            self.placeHolderText = placeHolderText
            self.placeHolderColor = placeHolderColor
            self.placeHolderFont = placeHolderFont
        }
    }
    
    
}

extension CustomTextView : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == self.placeHolderText{
            textView.text = ""
            textView.textColor = .black
            textView.font = placeHolderFont
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            textView.resignFirstResponder()
        }
        return true
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            textView.text = placeHolderText
            textView.textColor = placeHolderColor
            textView.font = placeHolderFont
        }
    }
    
}

