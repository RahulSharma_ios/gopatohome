//
//  MoreServicesView.swift
//  GoPatoHome
//
//  Created by 3Embed on 15/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

class MoreServicesView: UIView {

    @IBOutlet weak var rightViewButton: UIButton!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var leftImageView: UIView!
    
    @IBOutlet weak var stackHeight: NSLayoutConstraint!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var tf_AddingTasks: UITextField!
    @IBOutlet weak var leftImage: UIImageView!
    
    @IBOutlet weak var lblServiceName: UILabel!
    
    
    @IBOutlet weak var lblServiceDescription: UILabel!
    
    @IBOutlet var containerView: UIView!
    
    @IBOutlet weak var rightImageView: UIView!
   
    @IBOutlet weak var rightImage: UIImageView!
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        setupView()
        
        
    }
    
    
    func commonInit() {
        Bundle.main.loadNibNamed("MoreServicesView", owner: self, options: nil)
        containerView.fixInView(self)
    }
    
    func setupView(){
        
        innerView.layer.cornerRadius = 10.0
        innerView.addShadow(offset: CGSize(width: 0.0, height: 0.0), color: .lightGray, opacity: 0.7, radius: 2)
        
    }
    
    
}













