//
//  TaskImageView.swift
//  GoPatoHome
//
//  Created by 3Embed on 12/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

protocol   TaskImageViewDelegate  : class {
    func pressedTaksViewButton(view : TaskImageView )
}

class TaskImageView: UIView {

    var taksType = ""
    weak var delegate : TaskImageViewDelegate?
    @IBOutlet weak var lbl_TasksCounter: UILabel!
    
    @IBOutlet var containerView: UIView!
    
    @IBOutlet weak var taskImageView: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitView()
    }
    
    
    private func commitView(){
        Bundle.main.loadNibNamed("TaskImageView", owner: self, options: nil)
        containerView.fixInView(self)
        lbl_TasksCounter.layer.masksToBounds = true
        lbl_TasksCounter.layer.cornerRadius = lbl_TasksCounter.frame.height/2
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commitView()
    }
    
    @IBAction func pressedButton(_ sender: UIButton) {
        delegate?.pressedTaksViewButton(view : self)
    }
    
    
    
    
    
    
}
