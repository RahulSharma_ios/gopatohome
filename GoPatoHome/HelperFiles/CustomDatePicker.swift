
//  CustomDatePicker.swift
//  RMind
//
//  Created by Comvision on 04/07/18.
//  Copyright © 2018 Comvision. All rights reserved.
//

import UIKit
protocol CustomDatePickerDelegate{
    func updateDate(date : String)
}


class CustomDatePicker {
    private var vc  : UIViewController!
    var delegate : CustomDatePickerDelegate?
    init(vc : UIViewController , sender : UIButton){
        self.vc = vc
        
        setUpDatePicker(sender: sender)
    }
    
    private func setUpDatePicker(sender : UIButton ) {
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y:0, width: UIScreen.main.bounds.width , height: 260))
        
//        datePicker.subviews[1].isHidden = true
//         datePicker.subviews[2].isHidden = true
        print(datePicker.frame , vc.view.frame)
        datePicker.datePickerMode = UIDatePicker.Mode.date

        //add target
       // datePicker.addTarget(self, action: #selector(dateSelected(datePicker:sender:)), for: UIControlEvents.valueChanged)
        
        //add to actionsheetview
        let alertController = UIAlertController(title: "Date Selection", message:" " , preferredStyle: UIAlertController.Style.alert)
        
        alertController.view.addSubview(datePicker)//add subview
        
        let cancelAction = UIAlertAction(title: "Done", style: .cancel) { (action) in
            self.dateSelected(datePicker: datePicker, sender: sender)
        }
        
        //add button to action sheet
        alertController.addAction(cancelAction)

        let height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 300)
        alertController.view.addConstraint(height)
        
        let width :NSLayoutConstraint = NSLayoutConstraint(item: alertController.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.width)
        alertController.view.addConstraint(width)
        
        
        vc.present(alertController, animated: true, completion: nil)
        
    }
    @objc func dateSelected(datePicker:UIDatePicker , sender : UIButton)  {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
//        dateFormatter.dateStyle = DateFormatter.Style.short
        let selectedDate = dateFormatter.string(from: datePicker.date)
        sender.setTitle(selectedDate, for: .normal)

        delegate?.updateDate(date: selectedDate)
    }
    
    
}
