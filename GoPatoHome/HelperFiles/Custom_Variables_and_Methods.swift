//
//  Custom_Variables_and_Methods.swift
//  FICCIFLO
//
//  Created by Comvision on 06/07/18.
//  Copyright © 2018 Comvision. All rights reserved.
//

import UIKit
import SystemConfiguration
class Commom_Variables_and_Methods{
    fileprivate static var Obj_Commom_Variables_and_Methods : Commom_Variables_and_Methods!
    let CUSTOM_ERROE_RESPONSE = 99
    
    static let Google_Place_API_KEY = "AIzaSyABLUB2JohKPGRvYh55bt3NyCEC8U_0zWM"
    static let key_FIRSTNAME = "F_Name"
    static let key_LASTNAME = "L_Name"
    static let key_EMAIL = "Email"
    static let key_MOBILE = "Mobile"
    static let key_HO_MEMBER = "Member_Of_HO"
    static let key_PLACE = "Place"
    static let key_AREA = "area"
    static let key_DEVICE_ID = "device_Token"
    static let key_USER_ID = "user_id"
    static let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    private init(){
        
    }
    
    func set_DataToUserDefault(key : String , value : String){
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func get_DataFromUserDefault(key : String , defaulValue : String) -> String{
        
        if (UserDefaults.standard.value(forKey: key) != nil){
            return UserDefaults.standard.value(forKey: key) as! String
        }
        return defaulValue
    }
    
    
    func get_currentTimeStamp() -> String {
        
        return "\(Date().timeIntervalSince1970)"
    }
    
    
    
    
    public static func getInstance()-> Commom_Variables_and_Methods{
        if Obj_Commom_Variables_and_Methods == nil{
            Obj_Commom_Variables_and_Methods = Commom_Variables_and_Methods()
        }
        return Obj_Commom_Variables_and_Methods
    }
    
    
    func errorAlert(title : String , message : String ,vc : UIViewController , preferredStyle : UIAlertController.Style  ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        let cancelbtn = UIAlertAction(title: "OK", style: .default , handler: nil)
        alert.addAction(cancelbtn)
        vc.present(alert, animated: true, completion: nil)
    }
    
    func presentTost(title : String , message : String ,vc : UIViewController , delayTime : Double){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        vc.present(alert, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now() + delayTime) { [weak vc] in
                guard vc?.presentedViewController == alert else { return }
                vc?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
   
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "dd/MM/yyyy"
        
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
        
    }
    
    
    func getCurrentDate() -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
    
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    
    func convertedEventDate(stringDate : String , format : String) -> String{
        
        
        let string = stringDate
        
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: string)!
        dateFormatter.dateFormat =  format // "yyyy-MM-dd"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        print("EXACT_DATE : \(dateString)")
        return dateString
        
    }
    
    func convertStringtoArray(str : String ,separatedBy : String ) -> [String]{
        
        return (str.components(separatedBy: separatedBy))
        
    }
    
    
    
    func getAlert(vc : UIViewController , title : String , msg : String, completion : @escaping (UIAlertAction)  -> Void)  {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Ok", style: .cancel , handler: completion)
        alert.addAction(okButton)
        vc.present(alert, animated: true, completion: nil)
    }
    
    
    
    func getDecisionAlert(vc : UIViewController , title : String ,Ok_Title : String , msg : String, completion : @escaping (UIAlertAction)  -> Void)  {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okButton = UIAlertAction(title: Ok_Title, style: .cancel , handler: completion)
        alert.addAction(okButton)
        let cancelbtn = UIAlertAction(title: "Cancel", style: .default , handler: nil)
        alert.addAction(cancelbtn)
        vc.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    func getAppName() -> String{
        let infoDictionary: NSDictionary = (Bundle.main.infoDictionary as NSDictionary?)!
        let appName: NSString = infoDictionary.object(forKey: "CFBundleName") as! NSString
        NSLog("Name \(appName)")
        return appName as String
    }
    
    
    
    
}
