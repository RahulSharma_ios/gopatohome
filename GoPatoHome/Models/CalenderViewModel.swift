//
//  CalenderViewModel.swift
//  GoPatoHome
//
//  Created by 3Embed on 19/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import Foundation

class CalenderViewModel {
    
    private var boolArray : [Bool]!
    private var date : String!
    private var tasksCounter : [String]!
    private var shadowOn : Bool = false
    private var indexValue : Int!
    
    init(boolArray : [Bool] ,date : String , tasksCounter : [String] , indexValue : Int) {
        self.boolArray = boolArray
        self.date = date
        self.tasksCounter = tasksCounter
        self.indexValue = indexValue
    }
    
    func getBoolArray() -> [Bool]{
        return boolArray
    }
    
    func getDate() -> String {
        return date
    }
    func getTasksCounter() -> [String]{
        return tasksCounter
    }
    
    func getShodow() -> Bool {
        return shadowOn
    }
    
    func setShadowOn( shadowOn: Bool){
        self.shadowOn = shadowOn
    }

    func getIndexValue() -> Int{
        return indexValue
    }
}

