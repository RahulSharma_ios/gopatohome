//
//  HeaderWithImageModel.swift
//  GoPatoHome
//
//  Created by 3Embed on 19/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit
class HeaderWithImageModel{
    
    private var taskViewIsHidden : Bool!
    private var taskViewImage : UIImage!
    private var subText : String!
    private var imageShow : Bool!
    private var image : UIImage!
    private var text : String!
    private var headerBackGround : UIColor!
    private var animatedText : Bool!
    private var timeDuration : Double!
    private var delayTime : Double!
    
    private var tintColor: UIColor!
    private var textColor: UIColor!
    private var textFont: UIFont!
    private var subTextFont: UIFont!
    private var subTextColor: UIColor!
    
    private var buttonTag : Int8!
    
    init(taskViewIsHidden : Bool, taskViewImage : UIImage, taskCounter : String,  imageShow : Bool , image : UIImage, headerText : String, headerBackGround : UIColor,animatedText : Bool,timeDuration : Double,delayTime : Double  , tintColor: UIColor  , textColor: UIColor , textFont: UIFont , subTextFont: UIFont , subTextColor: UIColor ) {
        
        
        self.taskViewIsHidden = taskViewIsHidden
        self.taskViewImage = taskViewImage
        self.subText = taskCounter
    
        self.imageShow = imageShow
        self.image = image
        self.text = headerText
        self.headerBackGround = headerBackGround
        self.animatedText = animatedText
        self.timeDuration = timeDuration
        self.delayTime = delayTime
        
        
        self.tintColor = tintColor
        self.textColor = textColor
        self.textFont = textFont
        self.subTextFont = subTextFont
        self.subTextColor = subTextColor
        
        
    }
    
    func setButtonTag(buttonTag : Int8){
        self.buttonTag = buttonTag
    }

    func getButtonTag() -> Int8{
        guard  buttonTag != nil else {
            return 0
        }
        
        return buttonTag
    }
    
    
    func getHeaderElemets() -> (  Bool,  UIImage, String , Bool , UIImage,  String,  UIColor,  Bool,  Double,  Double  , UIColor  , UIColor ,  UIFont ,  UIFont ,  UIColor   ) {
        return ( taskViewIsHidden , taskViewImage , subText  , imageShow  , image,  text , headerBackGround ,animatedText ,timeDuration ,delayTime  , tintColor  , textColor , textFont, subTextFont , subTextColor)
    }
    
}
