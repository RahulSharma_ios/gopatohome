//
//  TextBasicModel.swift
//  GoPatoHome
//
//  Created by 3Embed on 20/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

class TextBasicModel {
    private var tintColor: UIColor!
    private var bgColor: UIColor!
    private var textColor: UIColor!
    private var image: UIImage!
    private var text: String!
    private var subText: String!
    private var textFont: UIFont!
    private var subTextFont: UIFont!
    private var subTextColor: UIColor!
    private var imageHidden : Bool!
    private var subtextHidden : Bool!
    private var placeholder : String!
    private var sizeValue : CGFloat!
    private var rightImage : UIImage!
    private var tagValue : Int8!

    init(tintColor: UIColor ,bgColor: UIColor, textColor: UIColor, image: UIImage, text: String, subText: String, textFont: UIFont, subTextFont: UIFont, subTextColor: UIColor , sizeValue : CGFloat , rightImage : UIImage) {
        
        self.tintColor = tintColor
        self.bgColor = bgColor
        self.textColor = textColor
        self.image = image
        self.text = text
        self.subText = subText
        self.textFont = textFont
        self.subTextFont = subTextFont
        self.subTextColor = subTextColor
        self.sizeValue = sizeValue
        self.rightImage = rightImage
    }
    
    
    init(text : String , textFont : UIFont , textColor : UIColor, imageHidden : Bool , subtextHidden : Bool , image : UIImage , placeholder : String  , tagValue : Int8 ) {
        
        self.text = text
        self.textFont = textFont
        self.textColor = textColor
        self.imageHidden = imageHidden
        self.subtextHidden = subtextHidden
        self.image = image
        self.placeholder = placeholder
        self.tagValue = tagValue
        
    }
    
    func getDashboardBasicModel() ->  ( UIColor , UIColor,  UIColor,  UIImage,  String,  String,  UIFont,  UIFont, UIColor , CGFloat , UIImage){
        
        return (tintColor ,bgColor, textColor, image, text, subText, textFont, subTextFont, subTextColor , sizeValue , rightImage)
    }
    
    func getTextBasicModelElements() -> (String , UIFont ,  UIColor,  Bool , Bool , UIImage , String  , Int8){
        return (text  , textFont  , textColor , imageHidden , subtextHidden ,  image , placeholder , tagValue)
    }

    
    
    
    
}


