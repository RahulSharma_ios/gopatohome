//
//  Theme.swift
//  GoPatoHome
//
//  Created by 3Embed on 13/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

struct Theme {
    
    static let shared: Theme = Theme()

    // Fonts
    let headerTextFontBlack = UIFont.getHeaderTextFontBlack() // GoPaTo Home Font
    let calanderTextFont = UIFont.getCalanderTextFont() // calander date font
    let selectedDateTextFont = UIFont.getHeaderTextFontBlack()// selected calander date font
    let dateTextFont = UIFont.getDateTextFont()
    let taskStatusTextFont =  UIFont.getTaskStatusTextFont()
    let sectionHeaderTextFont = UIFont.getSectionHeaderTextFont()
    let subTextFont = UIFont.getSubTextFont()
    let calanderSelectionTextFont = UIFont.getCalanderSelectionTextFont()
    let headerTextFont = UIFont.getHeaderTextFont() // Home Appointment
    let taskNameTextFont = UIFont.getTaskNameTextFont()
    
    
    
    // MARK: - COLORS
    let primaryColor = UIColor(red: 28, green: 89, blue: 130)
    let primaryLightColor =  UIColor(red: 79, green: 173, blue: 219)
    let primaryGrayColor = UIColor(red: 238, green: 238, blue: 239)
    let primaryDarkColor = UIColor(red: 23, green: 71, blue: 119)
    let primaryTextColor = UIColor.white
    let secondaryColor = UIColor(rgb: 0xEF9844)
    let secondaryLightColor = UIColor(rgb: 0x8eacbb)
    let secondaryDarkColor = UIColor(rgb: 0x34515e)
    let secondaryTextColor = UIColor.init(rgb: 0xedf0f2)
    let orangeColor = UIColor(rgb: 0xF99C31)
    
    
    let homeImageColor =  UIColor.getUIColor(color: "FFC736")
    let supermarketImageColor = UIColor.getUIColor(color: "F95C5C")
    let pharmaImageColor = UIColor.getUIColor(color: "85B3F8")
    let serviceIamgeColor = UIColor.getUIColor(color: "F69C33")
    let allowViewColor = UIColor.getUIColor(color: "4C82EE")
    let customGrayColor = UIColor.getUIColor(color: "B4B8D2")
    let tableViewBackGround = UIColor.getUIColor(color: "F2F6F8")
    
    
}



extension UIFont {
    
    class func getHeaderTextFontBlack() -> UIFont{
        if UIDevice.current.screenType == .iPhone5 {
            return UIFont(name: "Roboto-Black", size: 21)!
        }
        return UIFont(name: "Roboto-Black", size: 24)!
    }
    
    
    class func getHeaderTextFont() -> UIFont{
        if UIDevice.current.screenType == .iPhone5 {
            return UIFont(name: "Roboto-Black", size: 26)!
        }
        return UIFont(name: "Roboto-Black", size: 30)!
    }
    

    
    class func getCalanderTextFont() -> UIFont{
        if UIDevice.current.screenType == .iPhone5 {
            return UIFont(name: "Roboto-Regular", size: 14)!
        }
        return UIFont(name: "Roboto-Regular", size: 17)!
    }
    
    
    
    class func getTaskNameTextFont() -> UIFont{
        if UIDevice.current.screenType == .iPhone5 {
            return UIFont(name: "Roboto-Medium", size: 13)!
        }
        return UIFont(name: "Roboto-Medium", size: 16)!
    }
    
    
    class func getDateTextFont() -> UIFont{
        if UIDevice.current.screenType == .iPhone5 {
            return UIFont(name: "Roboto-Regular", size: 11)!
        }
        return UIFont(name: "Roboto-Regular", size: 14)!
    }
    
    
    
    class func getTaskStatusTextFont() -> UIFont{
        if UIDevice.current.screenType == .iPhone5 {
            return UIFont(name: "Roboto-Black", size: 13)!
        }
        return UIFont(name: "Roboto-Black", size: 16)!
    }
    
    
    
    class func getSectionHeaderTextFont() -> UIFont{
        if UIDevice.current.screenType == .iPhone5 {
            return UIFont(name: "Roboto-Medium", size: 17)!
        }
        return UIFont(name: "Roboto-Medium", size: 20)!
    }
    
    class func getSubTextFont() -> UIFont{
        if UIDevice.current.screenType == .iPhone5 {
            return UIFont(name: "Roboto-Regular", size: 13)!
        }
        return UIFont(name: "Roboto-Regular", size: 16)!
    }
    
  
    class func getCalanderSelectionTextFont() -> UIFont{
        if UIDevice.current.screenType == .iPhone5 {
            return UIFont(name: "Roboto-Medium", size: 11)!
        }
        return UIFont(name: "Roboto-Medium", size: 14)!
    }
}

