//
//  HomeTaskAddingVC.swift
//  GoPatoHome
//
//  Created by 3Embed on 20/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

class HomeTaskAddingVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    /// This is for configure To append the the custom tableView Cell for displaying in the  tableView
    private var configurators: [CellConfigurator] = []
    
    private var homeTasksArray : [TextBasicModel] = []
    private var supermarketTasksArray : [TextBasicModel] = []
    private var pharmaArray : [TextBasicModel] = []
    private var serviceArray : [TextBasicModel] = []
    
    private var  common_Variables_and_Methods  :   Common_Variables_and_Methods!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        common_Variables_and_Methods =  Common_Variables_and_Methods.getInstance()

        self.setupView()
        self.registerCells()
    }
    
    fileprivate func setupView(){
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
    }
    
    
    fileprivate func registerCells() {
        
        self.tableView.register( UINib.init(nibName: "HeaderWithImageTVCell", bundle: nil), forCellReuseIdentifier: "HeaderWithImageTVCell")
        
        
        self.tableView.register( UINib.init(nibName: "DatePickerTVCell", bundle: nil), forCellReuseIdentifier: "DatePickerTVCell")
        
        self.tableView.register( UINib.init(nibName: "BlankTVCell", bundle: nil), forCellReuseIdentifier: "BlankTVCell")
        
          self.tableView.register( UINib.init(nibName: "HomeTasksHolderTVCell", bundle: nil), forCellReuseIdentifier: "HomeTasksHolderTVCell")
        
         self.tableView.register( UINib.init(nibName: "AddingTasksTVCell", bundle: nil), forCellReuseIdentifier: "AddingTasksTVCell")
        
        
         self.tableView.register( UINib.init(nibName: "MoreServicesTVCell", bundle: nil), forCellReuseIdentifier: "MoreServicesTVCell")
        
          self.tableView.register( UINib.init(nibName: "CommentTVCell", bundle: nil), forCellReuseIdentifier: "CommentTVCell")
        
        createConfigurators()
        
        
    }
   
    fileprivate func createConfigurators() {
        self.configurators.removeAll(keepingCapacity: false)
        
    self.configurators.append(headerWithImageTVCellConfigurator(item: HeaderWithImageModel(taskViewIsHidden: true , taskViewImage: UIImage(), taskCounter: "5", imageShow: true , image: #imageLiteral(resourceName: "nesty1"), headerText: "Home Appointment", headerBackGround: .white, animatedText: true, timeDuration: 0.5, delayTime: 0.0, tintColor: .white, textColor: .black, textFont: Theme.shared.headerTextFont, subTextFont: UIFont(), subTextColor: UIColor() )))
        
        
        self.configurators.append(datePickerTVCellConfig(item: ("Monday Feb 10th", "Morning", UIColor.lightGray, #imageLiteral(resourceName: "Calander"))))
        
        
        self.configurators.append(blankTVCellConfig(item: (UIColor.white, "No tenes ninguna tarea agendada a este día. ¿Querés agendar una?", UIColor.lightGray)))
        
        
    self.configurators.append(headerWithImageTVCellConfigurator(item: HeaderWithImageModel(taskViewIsHidden: false  , taskViewImage: #imageLiteral(resourceName: "tareas"), taskCounter: "1", imageShow: false  , image: #imageLiteral(resourceName: "Home"), headerText: "Tarea del hogar", headerBackGround: .white, animatedText: true, timeDuration: 0.0, delayTime: 0.0, tintColor: .white, textColor: .black, textFont: Theme.shared.sectionHeaderTextFont, subTextFont: UIFont(), subTextColor: UIColor() )))
        
    
        self.addElementsToHomeTasks()
        
        self.configurators.append(homeTasksHolderConfig(item: homeTasksArray))
        
        self.configurators.append(addingTasksTVCellConfig(item: TextBasicModel.init(text: "", textFont: UIFont(), textColor: .white, imageHidden: false , subtextHidden: true, image: #imageLiteral(resourceName: "Add"), placeholder: "Agregar producto" , tagValue:  1)))
        
        self.configurators.append(blankTVCellConfig1(item: (UIColor.white, "", UIColor.lightGray)))
        
        
    self.configurators.append(headerWithImageTVCellConfigurator(item: HeaderWithImageModel(taskViewIsHidden: false  , taskViewImage: #imageLiteral(resourceName: "supermarket"), taskCounter: "1", imageShow: false  , image: #imageLiteral(resourceName: "Home"), headerText: "Refill de supermercado", headerBackGround: .white, animatedText: true, timeDuration: 0.0, delayTime: 0.0, tintColor: .white, textColor: .black, textFont: Theme.shared.sectionHeaderTextFont, subTextFont: UIFont(), subTextColor: UIColor() )))
        
        
        addElementsToSupermarket()
        for item in supermarketTasksArray{
            self.configurators.append(moreServicesTVCellConfig(item: item))
        }
        
        self.configurators.append(addingTasksTVCellConfig(item: TextBasicModel.init(text: "", textFont: UIFont(), textColor: .white, imageHidden: false , subtextHidden: true, image: #imageLiteral(resourceName: "Add"), placeholder: "Add Product" , tagValue:  2)))

        
        self.configurators.append(blankTVCellConfig1(item: (UIColor.white, "    ", UIColor.lightGray)))
        
        self.configurators.append(headerWithImageTVCellConfigurator(item: HeaderWithImageModel(taskViewIsHidden: false  , taskViewImage: #imageLiteral(resourceName: "pharma"), taskCounter: "1", imageShow: false  , image: #imageLiteral(resourceName: "Home"), headerText: "Refill de Farmacia", headerBackGround: .white, animatedText: true, timeDuration: 0.0, delayTime: 0.0, tintColor: .white, textColor: .black, textFont: Theme.shared.sectionHeaderTextFont, subTextFont: UIFont(), subTextColor: UIColor() )))
        
        addElementsToPharma()
        for item in pharmaArray{
            self.configurators.append(moreServicesTVCellConfig(item: item))
        }
        self.configurators.append(addingTasksTVCellConfig(item: TextBasicModel.init(text: "", textFont: UIFont(), textColor: .white, imageHidden: false , subtextHidden: true, image: #imageLiteral(resourceName: "Add"), placeholder: "Add Product" , tagValue:  3)))
        
        
        self.configurators.append(blankTVCellConfig1(item: (UIColor.white, "    ", UIColor.lightGray)))
        self.configurators.append(headerWithImageTVCellConfigurator(item: HeaderWithImageModel(taskViewIsHidden: false  , taskViewImage: #imageLiteral(resourceName: "pharma"), taskCounter: "1", imageShow: false  , image: #imageLiteral(resourceName: "Home"), headerText: "Refill de Farmacia", headerBackGround: .white, animatedText: true, timeDuration: 0.0, delayTime: 0.0, tintColor: .white, textColor: .black, textFont: Theme.shared.sectionHeaderTextFont, subTextFont: UIFont(), subTextColor: UIColor() )))
        
        addElementsToService()
        for item in serviceArray{
            self.configurators.append(moreServicesTVCellConfig(item: item))
        }
        
        self.configurators.append(blankTVCellConfig1(item: (UIColor.white, "    ", UIColor.lightGray)))

        let headerWithImage = HeaderWithImageModel(taskViewIsHidden: true  , taskViewImage: #imageLiteral(resourceName: "Lists"), taskCounter: "", imageShow: true  , image: #imageLiteral(resourceName: "Lists"), headerText: "Comentarios", headerBackGround: .white, animatedText: true, timeDuration: 0.0, delayTime: 0.0, tintColor: .white, textColor: .black, textFont: Theme.shared.sectionHeaderTextFont, subTextFont: UIFont(), subTextColor: UIColor())
        headerWithImage.setButtonTag(buttonTag: 1)
        
        self.configurators.append(headerWithImageTVCellConfigurator(item: headerWithImage ))
        
        self.configurators.append(commentTVCellConfig(item: ("Agregar Comentario", .lightGray, Theme.shared.taskNameTextFont)))
        
            self.configurators.append(blankTVCellConfig1(item: (UIColor.white, "    ", UIColor.lightGray)))
        
        self.tableView.reloadData()
        
    }
    
    
    private func addElementsToSupermarket(){
        self.supermarketTasksArray.append(TextBasicModel.init(tintColor: .white, bgColor: .white, textColor: .black, image: #imageLiteral(resourceName: "HappyFace"), text: "Pato Stores", subText: "Products", textFont: Theme.shared.taskNameTextFont, subTextFont: Theme.shared.subTextFont, subTextColor: .lightGray , sizeValue: 40.0 , rightImage:  #imageLiteral(resourceName: "pause")))
        
        self.supermarketTasksArray.append(TextBasicModel.init(tintColor: .white, bgColor: .white, textColor: .black, image: #imageLiteral(resourceName: "HappyFace"), text: "Pato Stores", subText: "Products", textFont: Theme.shared.taskNameTextFont, subTextFont: Theme.shared.subTextFont, subTextColor: .lightGray , sizeValue :40.0 , rightImage:  #imageLiteral(resourceName: "pause")))
        
        
    }
    
    
    
    private func addElementsToPharma(){
        self.pharmaArray.append(TextBasicModel.init(tintColor: .white, bgColor: .white, textColor: .black, image: #imageLiteral(resourceName: "HappyFace"), text: "Preescripciones", subText: "3 Products", textFont: Theme.shared.taskNameTextFont, subTextFont: Theme.shared.subTextFont, subTextColor: .lightGray , sizeValue: 45.0 , rightImage:  #imageLiteral(resourceName: "pause")))
        
        self.pharmaArray.append(TextBasicModel.init(tintColor: .white, bgColor: .white, textColor: .black, image: #imageLiteral(resourceName: "HappyFace"), text: "Productos de Bebe", subText: "3 Products", textFont: Theme.shared.taskNameTextFont, subTextFont: Theme.shared.subTextFont, subTextColor: .lightGray , sizeValue :45.0 , rightImage:  #imageLiteral(resourceName: "pause")))
        
        
    }
    
    
    private func addElementsToService(){
        self.serviceArray.append(TextBasicModel.init(tintColor: .white, bgColor: .white, textColor: .black, image: #imageLiteral(resourceName: "fotico"), text: "PDry cleaning", subText: "Description", textFont: Theme.shared.taskNameTextFont, subTextFont: Theme.shared.subTextFont, subTextColor: .lightGray , sizeValue: 45.0 , rightImage:  #imageLiteral(resourceName: "pause")))
        
        self.serviceArray.append(TextBasicModel.init(tintColor: .white, bgColor: .white, textColor: .black, image: #imageLiteral(resourceName: "fotico2"), text: "Fresh bread", subText: "Description", textFont: Theme.shared.taskNameTextFont, subTextFont: Theme.shared.subTextFont, subTextColor: .lightGray , sizeValue :45.0, rightImage:  #imageLiteral(resourceName: "pause")))
         self.serviceArray.append(TextBasicModel.init(tintColor: .white, bgColor: .white, textColor: .black, image: #imageLiteral(resourceName: "fotico1"), text: "Dog Walk", subText: "Description", textFont: Theme.shared.taskNameTextFont, subTextFont: Theme.shared.subTextFont, subTextColor: .lightGray , sizeValue :45.0 , rightImage:  #imageLiteral(resourceName: "pause"))  )
        
    }
    

    /// Adding elements to homeTasksArray collectoin , like Make the bed , Clean Kitchen etc
    
    private func addElementsToHomeTasks(){
        
        homeTasksArray.append(TextBasicModel.init(text: "Make the bed", textFont: Theme.shared.calanderSelectionTextFont, textColor: .black, imageHidden: true, subtextHidden: true,image: #imageLiteral(resourceName: "Check") , placeholder:  "" , tagValue:  0))
        
        homeTasksArray.append(TextBasicModel.init(text: "Clean Kitchen", textFont: Theme.shared.calanderSelectionTextFont, textColor: .black, imageHidden: true, subtextHidden: true,image: #imageLiteral(resourceName: "Check") , placeholder:  "" , tagValue:  0))
        
        homeTasksArray.append(TextBasicModel.init(text: "Clean Bathroom", textFont: Theme.shared.calanderSelectionTextFont, textColor: .black, imageHidden: true, subtextHidden: true , image: #imageLiteral(resourceName: "Check") , placeholder:  "" , tagValue:  0))
        
        homeTasksArray.append(TextBasicModel.init(text: "Home tyde", textFont: Theme.shared.calanderSelectionTextFont, textColor: .black, imageHidden: true, subtextHidden: true , image: #imageLiteral(resourceName: "Check") , placeholder:  "" , tagValue:  0))
        
        homeTasksArray.append(TextBasicModel.init(text: "Check on pets", textFont: Theme.shared.calanderSelectionTextFont, textColor: .black, imageHidden: true, subtextHidden: true, image: #imageLiteral(resourceName: "Check") , placeholder:  "" , tagValue:  0))
        
        //array_TextBasicModel.append(TextBasicModel.init(text: " Thew garbage\naway",textFont:Theme.shared.calanderSelectionTextFont, textColor: .black, imageHidden: true, subtextHidden: true, image: #imageLiteral(resourceName: "Check")))
       
    }
}


extension HomeTaskAddingVC : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.configurators[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        cell.backgroundColor = Theme.shared.tableViewBackGround
        cell.selectionStyle = .none
        if cell is AddingTasksTVCell{
            (cell as! AddingTasksTVCell).delegate = self
        } else if cell is HeaderWithImageTVCell{
            (cell as! HeaderWithImageTVCell).delegate = self
        }
        
        return cell
    }
   
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.configurators[indexPath.row] is headerWithImageTVCellConfigurator {
            return 50
        } else if self.configurators[indexPath.row] is datePickerTVCellConfig{
            return 50 
        }else if self.configurators[indexPath.row] is blankTVCellConfig{
            return 55
        }else if self.configurators[indexPath.row] is homeTasksHolderConfig{

            if homeTasksArray.count % 2 == 0{
                return (CGFloat(homeTasksArray.count * 60) / 2)
            }

            return (CGFloat((homeTasksArray.count + 1 ) * 60) / 2)
            
        }else if self.configurators[indexPath.row]  is addingTasksTVCellConfig {
            return 60
        }else if self.configurators[indexPath.row] is blankTVCellConfig1 {
            return 20
        }else if self.configurators[indexPath.row] is moreServicesTVCellConfig{
            return 65
        } else if self.configurators[indexPath.row] is commentTVCellConfig{
            return 200
        }

        return 0
    }
}


extension HomeTaskAddingVC : AddingTasksTVCellDelegate {
    func pressedAddTaskButton(cell: AddingTasksTVCell) {
        if cell.addTasksView.rightViewButton.tag == 1{
            print(cell.addTasksView.tf_AddingTasks.text)
            print("add elements to home task array")
        }else if  cell.addTasksView.rightViewButton.tag == 2{
            print("add elements to supermarket ")
             print(cell.addTasksView.tf_AddingTasks.text)
        }else if cell.addTasksView.rightViewButton.tag == 3{
            print("add elements toPharma")
             print(cell.addTasksView.tf_AddingTasks.text)
        }
    }
}



extension HomeTaskAddingVC : HeaderWithImageTVCellDelegate{
    func pressedbutton(cell: HeaderWithImageTVCell) {
        print("HeaderWithImageTVCellDelegate pressed")
    }
}
