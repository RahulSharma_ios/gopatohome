//
//  MainHomeScreen.swift
//  GoPatoHome
//
//  Created by 3Embed on 12/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit


class MainHomeScreen: UIViewController {
  
    private var calenderViewArray : [CalenderViewModel] = []

    
    @IBOutlet weak var homeScreenTV: UITableView!
    var configurators: [CellConfigurator] = []
    private var  common_Variables_and_Methods  :   Common_Variables_and_Methods!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        common_Variables_and_Methods =  Common_Variables_and_Methods.getInstance()
        
        
//        self.homeScreenTV.rowHeight = UITableView.automaticDimension
//        self.homeScreenTV.estimatedRowHeight = 200
        self.setupView()
        self.registerCells()
        self.homeScreenTV.backgroundColor = Theme.shared.tableViewBackGround
        
        self.setDataToView()
        
//        self.createConfigurators()
    }
    
    fileprivate func setupView(){
        self.homeScreenTV.dataSource = self
        self.homeScreenTV.delegate = self
        
    }
    
    
    
    private func setDataToView(){
        self.calenderViewArray.removeAll(keepingCapacity: false)
        for i in 0 ..< 7 {
            
            let temp_data = CalenderViewModel(boolArray: [false , false ,false , true], date: "Monday, February 1\(i)th", tasksCounter: ["\(i)","\(i * 1)","\(i)","0"] , indexValue: i)
            
            
            calenderViewArray.append(temp_data)
        }
        
        createConfigurators()
    }
    
    fileprivate func registerCells() {
       
        self.homeScreenTV.register( UINib.init(nibName: "HeaderWithImageTVCell", bundle: nil), forCellReuseIdentifier: "HeaderWithImageTVCell")
        
        self.homeScreenTV.register( UINib.init(nibName: "CollectionViewHolder", bundle: nil), forCellReuseIdentifier: "CollectionViewHolder")
        
        self.homeScreenTV.register( UINib.init(nibName: "CalenderHolderTVCell", bundle: nil), forCellReuseIdentifier: "CalenderHolderTVCell")
        
    }
  
    fileprivate func createConfigurators() {
        self.configurators.removeAll(keepingCapacity: false)
    
        self.configurators.append(headerWithImageTVCellConfigurator(item: HeaderWithImageModel(taskViewIsHidden: true, taskViewImage: UIImage(), taskCounter: "", imageShow: false , image: UIImage(), headerText: "GoPato Home", headerBackGround: Theme.shared.tableViewBackGround, animatedText: true, timeDuration: 0.5, delayTime: 0.0, tintColor: .white, textColor: .black, textFont: Theme.shared.headerTextFontBlack, subTextFont: UIFont(), subTextColor: UIColor() )))
        
        self.configurators.append(calenderHolderTVCell(item: calenderViewArray))
    
        self.configurators.append(collectionViewHolderTVCell(item: calenderViewArray))
        
      self.homeScreenTV.reloadData()
    }
    
}

extension MainHomeScreen : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.configurators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.configurators[indexPath.row]
        let cell = homeScreenTV.dequeueReusableCell(withIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        cell.backgroundColor = Theme.shared.tableViewBackGround
        cell.selectionStyle = .none
        if cell is CalenderHolderTVCell{
            (cell as! CalenderHolderTVCell).delegate = self
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.configurators[indexPath.row] is headerWithImageTVCellConfigurator {
            return 30
        } else if self.configurators[indexPath.row] is collectionViewHolderTVCell {
            return 180
        }else if self.configurators[indexPath.row] is calenderHolderTVCell{
            return 120
        }
        
        return 0
    }

}


extension MainHomeScreen : CalenderHolderTVCellDelegate{
    
    func selectDateForViewTasks(cell: CalanderCVCell) {
       
        for i in 0 ..< calenderViewArray.count{
            
            if cell.hiddenButton.tag == i {
                calenderViewArray[i].setShadowOn(shadowOn: true)
            }else{
                calenderViewArray[i].setShadowOn(shadowOn: false)
            }
        }
        createConfigurators()
        
        NotificationCenter.default.post(name: .scrollCalanderToIndex, object: nil, userInfo: ["rowData" : cell.hiddenButton.tag])
        
        
//        (name: .moveToAvatorView, object: nil
    }

}


extension Notification.Name {
    static let scrollCalanderToIndex = Notification.Name("scrollCalanderToIndex")
}
