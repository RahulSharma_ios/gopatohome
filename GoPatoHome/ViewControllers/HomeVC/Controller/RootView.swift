//
//  HomeVC.swift
//  GoPatoHome
//
//  Created by 3Embed on 11/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

class RootViewController: UIViewController{
    
    @IBOutlet weak var moreServicesTableView: UITableView!
    let cardViewHeight : CGFloat = 90
    
    let moreServices = ["Dry cleaning" , "Fresh bread" , "Dog Walk"]
    let imagesArray : [UIImage] = [#imageLiteral(resourceName: "fotico") , #imageLiteral(resourceName: "fotico2") , #imageLiteral(resourceName: "fotico1")]
    @IBOutlet weak var buttomBar: BottomViewOfApplication!
    
    
    @IBOutlet weak var lbl_SeeMore: UILabel!
    
    @IBOutlet weak var lbl_MoreServices: UILabel!
    
    @IBOutlet weak var lbl_Recents: UILabel!
    
    
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardViewHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak var arrowButton: UIButton!
    
    
    fileprivate var pagingViewControllers: [UIViewController] = []
    var headerHeightConstraint:NSLayoutConstraint!

    var visualEffectsView : UIVisualEffectView!
    var cardVisible = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        moreServicesTableView.rowHeight = 80.0
        moreServicesTableView.estimatedRowHeight = UITableView.automaticDimension
        
        
        buttomBar.delegate = self
        cardViewHeightAnchor.constant = cardViewHeight
        self.arrowButton.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
        arrowButton.addTarget(self, action: #selector(pressedArrowButton), for: .touchUpInside)
        moreServicesTableView.register(UINib.init(nibName: "MoreServicesTVCell", bundle: nil), forCellReuseIdentifier: "MoreServicesTVCell")
        setupView()
      
      //  hideCardViewObjects(value: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pressedArrowButton()
    }
    
    
    func hideCardViewObjects(value : Bool){
        
        lbl_Recents.isHidden = value
        lbl_SeeMore.isHidden = value
        lbl_MoreServices.isHidden = value
        
    }
    
    private func setupView(){
        
         cardView.addShadow(offset: CGSize(width: 0.0, height: 0), color: .lightGray, opacity: 0.7, radius: 4.0)
        
        cardView.layer.cornerRadius = 10
        cardView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        moreServicesTableView.dataSource = self 
        
    }
    
    
    @objc func  pressedArrowButton(){
        
        if cardVisible{
            
            UIView.animate(withDuration: 0.5, delay: 0.0, options: [.transitionFlipFromBottom], animations: {
                self.cardViewHeightAnchor.constant = self.cardViewHeight
                
                self.arrowButton.transform = CGAffineTransform(rotationAngle: (180 * .pi) / 180.0)
                self.view.layoutIfNeeded()
            })
            
        }else{
        
         UIView.animate(withDuration: 0.5, delay: 0.0, options: [.transitionFlipFromBottom], animations: {
                self.cardViewHeightAnchor.constant = (self.view.frame.height / 2 ) + 100
            self.arrowButton.transform = CGAffineTransform(rotationAngle: (360.0 * .pi) / 180.0)
             self.view.layoutIfNeeded()
        })
            
        }
        cardVisible = !cardVisible
    }

}



extension RootViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moreServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreServicesTVCell", for: indexPath) as! MoreServicesTVCell
        
        cell.dataSource(data: (taskName: moreServices[indexPath.row], desc: "Description", image: imagesArray[indexPath.row], playPauseImage: #imageLiteral(resourceName: "pause")))
        
        return cell
        
    }
}



extension RootViewController : BottomViewOfApplicationDelegate {
    func pressedLisrButton() {
        NotificationCenter.default.post(name: .moveToListView, object: nil)
    }
    
    func pressedHomeButton() {
      NotificationCenter.default.post(name: .moveToHomeView, object: nil)
    }
    
    func pressedChatButton() {
     NotificationCenter.default.post(name: .moveToChatView, object: nil)
    }
    
    func pressedAvatorButton() {
       NotificationCenter.default.post(name: .moveToAvatorView, object: nil)
    }
    
}



extension Notification.Name {
    static let moveToHomeView = Notification.Name("moveToHomeView")
    static let moveToChatView = Notification.Name("moveToChatView")
    static let moveToListView = Notification.Name("moveToListView")
    static let moveToAvatorView = Notification.Name("moveToAvatorView")
}


