//
//  MyPageViewController.swift
//  GoPatoHome
//
//  Created by 3Embed on 13/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

class MyPageViewController: UIPageViewController {

    
    
    lazy var orderedViewControllers : [UIViewController] = {
        return [self.newVC(viewController: "ViewController1"),
        self.newVC(viewController: "MainHomeScreen"),
        self.newVC(viewController: "ViewController2"),
        self.newVC(viewController: "ViewController3")
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    
        
        setupView()
        
        if let firstViewController = orderedViewControllers[1] as? UIViewController{
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    private func setupView(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToHomeView(_:)), name: .moveToHomeView, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToChatView(_:)), name: .moveToChatView, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToListView(_:)), name: .moveToListView, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToAvatorView(_:)), name: .moveToAvatorView, object: nil)
        
    }
    
    
    @objc func moveToHomeView(_ notification:Notification) {
        if let firstViewController = orderedViewControllers[1] as? UIViewController{
            setViewControllers([firstViewController], direction: .forward, animated: false , completion: nil)
        }
    }
    @objc func moveToChatView(_ notification:Notification) {
        if let firstViewController = orderedViewControllers[2] as? UIViewController {
            setViewControllers([firstViewController], direction: .forward, animated: false , completion: nil)
        }
    }
    @objc func moveToListView(_ notification:Notification) {
        if let firstViewController = orderedViewControllers.first{
            setViewControllers([firstViewController], direction: .forward, animated: false, completion: nil)
        }
    }
    @objc func moveToAvatorView(_ notification:Notification) {
        if let firstViewController = orderedViewControllers.last{
            setViewControllers([firstViewController], direction: .forward, animated: false , completion: nil)
        }
    }
    
    
    
    
    func newVC(viewController : String) -> UIViewController{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewController)
        
    }
    


}


