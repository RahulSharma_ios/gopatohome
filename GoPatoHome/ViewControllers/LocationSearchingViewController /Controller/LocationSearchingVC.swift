//
//  LocationSearchingVC.swift
//  GoPatoHome
//
//  Created by 3Embed on 05/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import CoreLocation
class LocationSearchingVC: UIViewController {

    var isAllowed = false
    private var buttonCount = 0
    
    // MARK:- button View outlets
    
    @IBOutlet weak var containerStackView: UIStackView!
    @IBOutlet weak var currentLocationView: UIView!
    
    @IBOutlet weak var currentLocationLbl: UILabel!
    
    @IBOutlet weak var bottomView: ButtonView!
    @IBOutlet weak var image_Left: UIView!
    @IBOutlet weak var image_Right: UIView!
    
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var btn_Allow: UIButton!
    
    @IBOutlet weak var btnLbl: UILabel!
    
    @IBOutlet weak var hiddenView: UIView!
    
    @IBOutlet weak var rightHook: NSLayoutConstraint!
    @IBOutlet weak var leftHook: NSLayoutConstraint!
     @IBOutlet weak var buttonView: UIView!
    
    private var  commom_Variables_and_Methods :  Common_Variables_and_Methods!
       let otherContainerViewHeight : CGFloat = 75.0
//    GMSServices.provideAPIKey(commom_Variables_and_Methods.Google_Place_API_KEY
    private var nameArray = [String]()
    private var formatted_addressArray = [String]()
    
    @IBOutlet weak var otherContainerViewHeightContraint: NSLayoutConstraint!
    let locationManager = CLLocationManager()
    @IBOutlet weak var myMapView: GMSMapView!
    
    @IBOutlet weak var stackViewBottomAncher: NSLayoutConstraint!
    
    @IBOutlet weak var tf_Location: UITextField!
    
    @IBOutlet weak var otherViewContainer: UIView!
    
    @IBOutlet weak var searchResultTableView: UITableView!
    
    @IBOutlet weak var viewWithButton: UIView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewWithSearchBar: UIView!
    
    @IBOutlet weak var stackViewLeftAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var stackViewRightAnchor: NSLayoutConstraint!
    
    
    
    
    @IBOutlet weak var blurView: UIView!
    
    
    @IBOutlet weak var smileImageView: UIImageView!
    
    @IBOutlet weak var lblView: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomView.delegate = self
        bottomView.blueViewTitleLbl.text = "Accept"
        bottomView.redViewTitleLbl.text = "Cancel"
        searchResultTableView.dataSource = self
        searchResultTableView.delegate = self
        commom_Variables_and_Methods = Common_Variables_and_Methods.getInstance()
        
        
        searchResultTableView.register(UINib(nibName: "SearchLocationTVCell", bundle: nil), forCellReuseIdentifier: "SearchLocationTVCell")
        
        self.myMapView.layer.opacity = 0.1
        self.setKeyBoardObserver()
        self.otherViewContainer.isHidden = true
        viewWithSearchBar.isHidden = true
        searchResultTableView.isHidden = true
        setupView()
        
        
        if commom_Variables_and_Methods.getStringData_From_UserDefaults(key: commom_Variables_and_Methods.key_ALLOWED_LOCATION_ACCESS) == "Y" {
            myMapView.isMyLocationEnabled = true
            self.checkLocationService()
            self.currentLocationView.isHidden = false
            self.viewWithButton.isHidden = true
            currentLocationView.bringSubviewToFront(myMapView)
            viewWithButton.bringSubviewToFront(myMapView)
            
        }else{
            self.viewWithButton.bringSubviewToFront(myMapView)
            //            self.currentLocationView.isHidden = true
            //            self.viewWithButton.isHidden = false
            //            self.buttonView.isHidden = true
        }
        
    }
    
    
    
    
    @IBAction func pressedHiddenButton(_ sender: UIButton) {
        
      
        viewWithButton.isHidden = true
        viewWithSearchBar.isHidden = false
        
        searchResultTableView.isHidden = true
        self.otherViewContainer.isHidden = false
        self.otherContainerViewHeightContraint.constant = otherContainerViewHeight
        tableViewHeight.constant = (self.view.frame.height / 3 )
        stackViewLeftAnchor.constant = 0.0
        stackViewRightAnchor.constant = 0.0
        tf_Location.becomeFirstResponder()
    }
    
    
    func updateLayoutWithViews(boolValue : Bool , anchorValue : CGFloat ){
        
        currentLocationView.isHidden = !boolValue
        
        containerStackView.isHidden = boolValue
        viewWithSearchBar.isHidden = boolValue
        otherViewContainer.isHidden = boolValue
        
        stackViewLeftAnchor.constant = anchorValue
        stackViewRightAnchor.constant = anchorValue
    }
    
    
    @IBAction func currentLocationViewButton(_ sender: UIButton) {
        self.tf_Location.becomeFirstResponder()
        
       updateLayoutWithViews(boolValue: false, anchorValue: 0.0)
        
//        otherContainerViewHeightContraint.constant =
        
    }
    
    
    @IBAction func buttonViewButton(_ sender: UIButton) {
        if btnLbl.text == "Accept"{
            self.performSegue(withIdentifier: "locationSearchToRangeVC", sender: self)
        }else{
            
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "locationSearchToRangeVC" {
            if let vc = segue.destination as? RangeVC {
                if let locationName = currentLocationLbl.text{
                      vc.locationName  = locationName
                }
            }
        }
        
    }
    
   
    
    func setupLocationManager(){
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
    }
    
    func checkLocationService(){
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
        }
    }

    private func setupView(){
        tf_Location.delegate = self
        
        tableViewHeight.constant = 0.0
        if UIDevice.current.screenType == .iPhone5{
            currentLocationLbl.font = UIFont.systemFont(ofSize: 13)
        }else{
             currentLocationLbl.font = UIFont.systemFont(ofSize: 16)
        }
        commom_Variables_and_Methods.getShadwoView_10radius(view: viewWithButton)
        commom_Variables_and_Methods.getShadwoView_10radius(view: currentLocationView)
        
        
        self.btn_Cancel.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner ]
        self.btn_Allow.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner ]
        
        commom_Variables_and_Methods.getRoundedCorner(view: buttonView , shadowEffect: true)
        commom_Variables_and_Methods.getRoundedCorner(view: btn_Allow , shadowEffect: true)
        commom_Variables_and_Methods.getRoundedCorner(view: btn_Cancel , shadowEffect: true)
        
        
        btn_Allow.addTarget(self, action: #selector(pressed_AllowBtn), for: .touchUpInside)
        btn_Cancel.addTarget(self, action: #selector(pressed_CancelBtn), for: .touchUpInside)
        
        viewWithSearchBar.layer.cornerRadius = (viewWithSearchBar.frame.height / 2 )
        viewWithSearchBar.layer.borderWidth = 1.5
        viewWithSearchBar.layer.borderColor = UIColor.getUIColor(color: "4C82EE").cgColor
        pressed_AllowBtn()
        mapViewAnimation()
        
        blurView.layer.opacity = 0.5
        onHideShowAnotherController(value: true)
    }
    
    private func mapViewAnimation(){
        
        UIView.animate(withDuration: 0.8, delay: 0.6,  animations: {
            self.myMapView.layer.opacity += 0.9
        }) { (finished ) in
            
        }
    }
    
    override func keyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo
        let keyboardSize =  info?["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        
        UIView.animate(withDuration: 0.6) {
            self.stackViewBottomAncher.constant  = keyboardSize.height
            
            
            self.view.layoutIfNeeded()
        }
        
//
        //self.scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: keyboardSize.height + 50 , right: 0)
    }
    
    override func keyboardWillHide(notification: NSNotification) {
        if self.stackViewBottomAncher.constant != 0.0 {
           
            self.updateLayoutWithViews(boolValue: true, anchorValue: 25.0)
           self.stackViewBottomAncher.constant = 0.0
               currentLocationView.isHidden = false
        }
    }
    
    @objc func pressed_AllowBtn(){
        
        
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.rightHook.constant = self.commom_Variables_and_Methods.defualthookValue
            self.leftHook.constant = self.commom_Variables_and_Methods.hookValue
            self.image_Right.isHidden = false
            self.image_Left.isHidden = true
            self.btn_Cancel.isHidden = false
            self.btn_Allow.isHidden = true
            self.buttonView.backgroundColor = .getUIColor(color: "4C82EE")
            self.btnLbl.text = "Accept"
            self.hiddenView.isHidden = false
        })
        
    }
    
    
    @objc func pressed_CancelBtn(){
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.leftHook.constant = self.commom_Variables_and_Methods.defualthookValue
            self.rightHook.constant = self.commom_Variables_and_Methods.hookValue
            self.image_Right.isHidden = true
            self.image_Left.isHidden = false
            self.btn_Allow.isHidden = false
            self.btn_Cancel.isHidden = true
            self.hiddenView.isHidden = true
            self.buttonView.backgroundColor = .getUIColor(color: "F95C5C")
            self.btnLbl.text = "Cancel"
        })
    }
    
}


extension LocationSearchingVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        searchResultTableView.isHidden = false
        
        self.searchPlaceFromGoogle(place: textField.text ?? "")
        return true
    }
    
    private func searchPlaceFromGoogle(place : String){
        print(place)
        
        var  strGoogleApi = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=\(place)&key=\(commom_Variables_and_Methods.Google_Place_API_KEY)".replacingOccurrences(of: " ", with: "+")

        
            strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        print(strGoogleApi)
        
        var urlRequest = URLRequest(url: URL(string: strGoogleApi )! )
        urlRequest.httpMethod =  "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error == nil {
                let jsonDict = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
          
                let dataDict = jsonDict as! Dictionary<String , AnyObject>
                
                print(dataDict)
                guard let resultArray = dataDict["results"] as? [NSDictionary] else{
                    return
                }
                
                print(resultArray)
                
                for innerDict in resultArray{
                
                    guard let name = innerDict["name"] as? String else  {
                        return
                    }
                    
                    guard let formatted_address = innerDict["formatted_address"] as? String else  {
                        return
                    }
                    
                    self.nameArray.append(name)
                    self.formatted_addressArray.append(formatted_address)

                }
                
                DispatchQueue.main.async {
                    if self.nameArray.count > 0 {
                         self.currentLocationLbl.text = self.nameArray[0] + "," + self.formatted_addressArray[0]
                    }
                   
                   //self.searchResultTableView.reloadData()
                }
                

            }else {
                print(error)
            }
        }
        task.resume()
    }
    
    
}


extension LocationSearchingVC : GMSMapViewDelegate , CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first{
            
            DispatchQueue.main.async{
                
                let position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude )
                
                let marker = GMSMarker(position: position)
                self.myMapView.camera = GMSCameraPosition(target: position, zoom: 12, bearing: 0, viewingAngle: 0)
               
                marker.map = self.myMapView
                
                
                self.commom_Variables_and_Methods.getAddressFromLatLon(pdblLatitude: location.coordinate.latitude, withLongitude: location.coordinate.longitude, successBlock: { (locationString) in
                    self.currentLocationLbl.text = locationString
                     marker.title = locationString
                })

            }
        }
        
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if (status == CLAuthorizationStatus.authorizedWhenInUse) {
            myMapView.isMyLocationEnabled = true
        }
        
    }
}


extension LocationSearchingVC : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if nameArray.count > 0{
            return nameArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchLocationTVCell", for: indexPath) as! SearchLocationTVCell
        
        if indexPath.row == 0 {
            cell.backButton.addTarget(self, action: #selector(backButton), for: .touchUpInside)
        }else{
             cell.backButton.isEnabled = false
            cell.backButton.setImage(UIImage(), for: .normal)
        }
        
        
        cell.setDataToView(data: (nameArray[indexPath.row] , formatted_addressArray[indexPath.row]))
        
        return cell
    }
    

    @objc func backButton(){
        self.dismiss(animated: true, completion: nil)
    }
}



extension LocationSearchingVC : ButtonViewDelegate{
    func pressedBlueButton() {
        if  buttonCount == 0{
            onHideShowAnotherController(value: false )
            buttonCount += 1
        }else if buttonCount == 1{
            
           self.performSegue(withIdentifier: "LocationVCToRegisterVC", sender: self)
        }
    
        
    }
    
    func pressedRedButton() {
      
    }
    
    func onHideShowAnotherController(value : Bool){
            blurView.isHidden = value
            lblView.isHidden = value
            smileImageView.isHidden = value
    }
    
}
