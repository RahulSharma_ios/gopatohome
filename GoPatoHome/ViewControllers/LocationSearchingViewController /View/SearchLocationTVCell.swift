//
//  SearchLocationTVCell.swift
//  GoPatoHome
//
//  Created by 3Embed on 09/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

class SearchLocationTVCell: UITableViewCell {

    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var lbl_Place: UILabel!
    
    @IBOutlet weak var lbl_PlaceDec: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lbl_PlaceDec.textColor = .getUIColor(color: "#9494AD")
        
        lbl_Place.textColor = .getUIColor(color: "#9494AD")
        // Initialization code
    }

  
    func setDataToView(data : (String , String)){
        
        lbl_Place.text = data.0
        lbl_PlaceDec.text = data.1
    }
    
}
