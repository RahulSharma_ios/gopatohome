//
//  LocationServiceReqVC.swift
//  GoPatoHome
//
//  Created by apple on 02/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class LocationServiceReqVC: UIViewController , GMSMapViewDelegate , CLLocationManagerDelegate{
    private var mDidFindMyLocation = false
    let locationManager = CLLocationManager()
    @IBOutlet weak var myMapView: GMSMapView!
    
    let hookValue : CGFloat = 48.0
    let defualthookValue : CGFloat = 20.0
    
    @IBOutlet weak var btn_Cancel: UIButton!
    
    @IBOutlet weak var image_Left: UIImageView!
    @IBOutlet weak var image_Right: UIImageView!
    
    @IBOutlet weak var btn_Allow: UIButton!
    
    @IBOutlet weak var rightHook: NSLayoutConstraint!
    
    @IBOutlet weak var leftHook: NSLayoutConstraint!
    
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var btnLbl: UILabel!
    
    @IBOutlet weak var hiddenView: UIView!
    
    
    @IBOutlet weak var customAlertView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        myMapView.isMyLocationEnabled = true
        
        self.checkLocationService()
    
    }
//    private func initializeMap(){
//
//
//        let camera = GMSCameraPosition.camera(withLatitude: 52.520736, longitude: 13.409423, zoom: 12)
//        self.myMapView.camera = camera
//
//    }
    

    @IBAction func pressedHiddenButton(_ sender: UIButton) {
        if btnLbl.text == "Allow"{
//            UIView.setAnimationsEnabled(false)
        }else if btnLbl.text == "Cancel" {
            print("Cancel")
        }
        
    }
    
    private func setupView(){
        
        myMapView.layer.opacity = 0.1
        buttonView.layer.cornerRadius = buttonView.frame.height / 2
        
        
        btn_Allow.layer.cornerRadius = btn_Cancel.frame.height / 2
        btn_Allow.layer.shadowColor = UIColor.gray.cgColor
        btn_Allow.layer.shadowRadius = 10.0
        btn_Allow.layer.shadowOpacity = 1.0
        btn_Allow.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        btn_Cancel.layer.cornerRadius = btn_Cancel.frame.height / 2
        btn_Cancel.layer.shadowColor = UIColor.gray.cgColor
        btn_Allow.layer.shadowRadius = 10.0
        btn_Allow.layer.shadowOpacity = 1.0
        btn_Allow.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        
        self.btn_Cancel.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner ]
        self.btn_Allow.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner ]
        
        btn_Allow.addTarget(self, action: #selector(pressed_AllowBtn), for: .touchUpInside)
        btn_Cancel.addTarget(self, action: #selector(pressed_CancelBtn), for: .touchUpInside)
        
        
        // MAAK:- alert View setting
    
        customAlertView.layer.cornerRadius = 33
        customAlertView.layer.shadowColor = UIColor.gray.cgColor
        customAlertView.layer.shadowRadius = 10
        customAlertView.layer.shadowOpacity = 1.0
        customAlertView.layer.shadowOffset = CGSize(width: 0, height: 2)
        mapViewAnimation()
        pressed_AllowBtn()

    }
    
    
    private func mapViewAnimation(){

        UIView.animate(withDuration: 0.8, delay: 1.0,  animations: {
            self.myMapView.layer.opacity += 0.9
        }) { (finished ) in
            
        }
    }
    
    
    @objc func pressed_AllowBtn(){
        

        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.rightHook.constant = self.defualthookValue
            self.leftHook.constant = self.hookValue
            self.image_Right.isHidden = false
            self.image_Left.isHidden = true
            self.btn_Cancel.isHidden = false
            self.btn_Allow.isHidden = true
            self.buttonView.backgroundColor = .getUIColor(color: "4C82EE")
            self.btnLbl.text = "Allow"
            self.hiddenView.isHidden = false
            
            
        })
        
    }
    
    
    @objc func pressed_CancelBtn(){
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.leftHook.constant = self.defualthookValue
            self.rightHook.constant = self.hookValue
            self.image_Right.isHidden = true
            self.image_Left.isHidden = false
            self.btn_Allow.isHidden = false
            self.btn_Cancel.isHidden = true
            self.hiddenView.isHidden = true
            self.buttonView.backgroundColor = .getUIColor(color: "F95C5C")
            self.btnLbl.text = "Cancel"
        })
    }
    
    func setupLocationManager(){
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
      
    }
    
    
    func checkLocationService(){
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
            locationManager.startUpdatingLocation()
        }else{
            let alert = UIAlertController(title: "Location Services disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                self.locationManager.requestWhenInUseAuthorization()
            }
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func checkLocationAuthorization() {
    
        switch CLLocationManager.authorizationStatus() {
    
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
         break

        case .denied :
            break
    
        case .restricted :
            let alert = UIAlertController(title: "Location Services disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                self.locationManager.requestWhenInUseAuthorization()
            }
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            break
            
        case .authorizedAlways:
            
            break
            
        case .authorizedWhenInUse:
            
            break
            
         default:
            break
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first{
        
            DispatchQueue.main.async {
                let position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude )
                
                let marker = GMSMarker(position: position)
                        self.myMapView.camera = GMSCameraPosition(target: position, zoom: 15, bearing: 0, viewingAngle: 0)
                        marker.title = "myLocation"
                    marker.map = self.myMapView
                }
            
            }
        
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if (status == CLAuthorizationStatus.authorizedWhenInUse) {
            myMapView.isMyLocationEnabled = true
        }
        
    }

}


