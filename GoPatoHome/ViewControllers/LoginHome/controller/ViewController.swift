//
//  ViewController.swift
//  GoPatoHome
//
//  Created by apple on 01/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit
import GoogleSignIn
import Google

class ViewController: UIViewController {
    
    
    
    private var  commom_Variables_and_Methods  :  Common_Variables_and_Methods!
    
 
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var view_Login: UIView!
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var bottomView: ButtonView!
    
    
    
    @IBOutlet weak var imageView_GoPaTo: UIImageView!
    @IBOutlet weak var image_Login: UIImageView!
    @IBOutlet weak var imageView_Home: UIImageView!
    
    @IBOutlet weak var btn_Facebook: UIButton!
    @IBOutlet weak var btn_Signup: UIButton!
    @IBOutlet weak var btn_Google: UIButton!
    @IBOutlet weak var locationViewBtn: UIButton!
    
    @IBOutlet weak var lbl_LifeRefill: UILabel!
  
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var alertViewHeight: NSLayoutConstraint!
    @IBOutlet weak var backGroundLeftAnchor: NSLayoutConstraint!
    @IBOutlet weak var backGroundRightAnchor: NSLayoutConstraint!
    @IBOutlet weak var backGroundBottomAnchor: NSLayoutConstraint!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationControllerHidden()
       
        locationViewBtn.addTarget(self, action: #selector(pressedLocationViewButton), for: .touchUpInside)
        commom_Variables_and_Methods =  Common_Variables_and_Methods.getInstance()
         buttonView.isHidden = true
        
        commom_Variables_and_Methods.getRoundedCorner(view: buttonView , shadowEffect: true)
        setupView()
        self.btn_Google.addTarget(self, action: #selector(pressedGoogleLoginButton), for: .touchUpInside)
    }

    @objc func pressedGoogleLoginButton(){
        googleSignInPressed()
    }
    
    
    @objc func pressedLocationViewButton(){
        
        UIView.animate(withDuration: 0.3) {
            self.alertView.isHidden = false
            self.alertViewHeight.constant = (self.view.frame.height / 2 ) + 50
            self.view.layoutIfNeeded()
        }
        
        UIView.animate(withDuration: 0.6) {
            self.self.backGroundLeftAnchor.constant = 0.0
            self.backGroundBottomAnchor.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    private func setupView(){
        
        alertView.isHidden = true
        alertView.layer.cornerRadius = 30
        alertView.clipsToBounds = true
        alertView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        alertViewHeight.constant = 0.0
        
        
        bottomView.delegate = self
        bottomView.blueViewTitleLbl.text = "Accept"
        bottomView.redViewTitleLbl.text = "Cancel"
        
        backGroundRightAnchor.constant = 180
        backGroundBottomAnchor.constant = 100
        
        self.imageView_GoPaTo.isHidden = true
        self.imageView_Home.isHidden = true
        self.view_Login.isHidden = true
        self.stackView.isHidden = true
        self.lbl_LifeRefill.isHidden = true
        self.imageView_GoPaTo.layer.opacity = 0.0
        self.imageView_Home.layer.opacity = 0.0
        self.view_Login.layer.opacity = 0.0
        self.stackView.layer.opacity = 0.0
        self.lbl_LifeRefill.layer.opacity = 0.0
        self.buttonView.layer.opacity = 0.0
        
        btn_Facebook.layer.cornerRadius = (btn_Facebook.frame.height / 2)
        btn_Signup.layer.cornerRadius = (btn_Signup.frame.height / 2 )
        btn_Google.layer.cornerRadius = (btn_Google.frame.height / 2 )
        view_Login.layer.cornerRadius = (view_Login.frame.height / 2 )
    
        UIView.animate(withDuration: 0.2, delay: 2.0 , animations: {

            self.imageView_GoPaTo.layer.opacity += 1.0
            self.imageView_Home.isHidden = false
            self.imageView_GoPaTo.isHidden = false
            self.imageView_Home.layer.opacity += 1.0
          
        }) {  (value) in
            if value {
                UIView.animate(withDuration: 0.1, delay: 0.5 , animations: {
                
                    self.view_Login.layer.opacity += 1.0
                    self.stackView.layer.opacity += 1.0
                    self.lbl_LifeRefill.layer.opacity += 1.0
                    
                    self.view_Login.isHidden = false
                    self.stackView.isHidden = false
                    self.lbl_LifeRefill.isHidden = false
                
                })
            }
        }
    }
    
    @IBAction func pressedLoginButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "homeToLogin", sender: self)
    }
    
    
    @IBAction func pressedSignUpButton(_ sender: UIButton) {
        
        UIView.transition(with: view, duration: 0.6,  options: .transitionCrossDissolve, animations: {
            self.view_Login.layer.opacity -= 1.0
            self.stackView.layer.opacity -= 1.0
            self.lbl_LifeRefill.layer.opacity -= 1.0
            
            self.view_Login.isHidden = true
            self.stackView.isHidden = true
            self.lbl_LifeRefill.isHidden = true
        })
        
    
        UIView.animate(withDuration: 0.7, delay: 0.0, animations: {
            
            
            self.view_Login.layer.opacity -= 1.0
            self.stackView.layer.opacity -= 1.0
            self.lbl_LifeRefill.layer.opacity -= 1.0
            
            self.view_Login.isHidden = true
            self.stackView.isHidden = true
            self.lbl_LifeRefill.isHidden = true
            
            
            self.backGroundRightAnchor.constant -= 180
            self.backGroundBottomAnchor.constant = 100
            self.backGroundLeftAnchor.constant -= 180
            self.view.layoutIfNeeded()
            
        }) { (value) in
            
            UIView.animate(withDuration: 0.5, delay: 0.1,  animations: {
                self.buttonView.isHidden = false
                self.buttonView.layer.opacity += 1.0
            }, completion: nil)
            
        }
        
//        self.performSegue(withIdentifier: "homeSignInLocation", sender: self)
        
    }

}

extension ViewController : GIDSignInDelegate, GIDSignInUIDelegate {
    
    func googleSignInPressed() {
        
        let googlesignin = GIDSignIn.sharedInstance()
        googlesignin?.clientID = self.getInfoPlist(fileName: "GoogleService-Info", indexString: "CLIENT_ID") as? String
        googlesignin?.scopes.append("https://www.googleapis.com/auth/plus.login")
        googlesignin?.scopes.append("https://www.googleapis.com/auth/plus.me")
        googlesignin?.uiDelegate = self
        googlesignin?.shouldFetchBasicProfile = true
        googlesignin?.delegate = self
        googlesignin?.signIn()
        
    }
    
    func getInfoPlist(fileName:String?,indexString:NSString) ->AnyObject?{
        
        let path = Bundle.main.path(forResource: fileName, ofType: "plist")
        let storedvalues = NSDictionary(contentsOfFile: path!)
        let response: AnyObject? = storedvalues?.object(forKey: indexString) as AnyObject?
        return response
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if (error == nil) {
            
            let givenName = user.profile.givenName as String
            let familyName = user.profile.familyName as String
            let email = user.profile.email as String
            let profile_id = user.userID as String
            
            let user = ["first_name":givenName,"last_name":familyName,"email":email,"action" : "SocialAuth","provider" : "Google","fcm_token" : "SaMpLeToKeN123456","device_id" : UIDevice.current.identifierForVendor?.uuidString ?? "","device_type" : "iOS","profile_id" : profile_id]
            
            print(user)
            
            //            self.doLOGIn(data: user)
        } else {
            print("\(error.localizedDescription)")
        }
        
        
    }
    
    //    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
    //        if error == nil{
    //        print(user.profile.email)
    //        }
    //        else{
    //            print("error" )
    //        }
    //    }
}


//extension ViewController : FBSDKLoginButtonDelegate{
//
//
//    func facebookLogIn() {
//
//        //        if isInternetAvailable() {
//
//        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
//
//        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
//
//            if (error == nil){
//
//                let fbloginresult : FBSDKLoginManagerLoginResult = result!
//                if fbloginresult.grantedPermissions != nil{
//
//                    if(fbloginresult.grantedPermissions.contains("email")){
//
//                        self.getFBUserData()
//                    }
//                }
//            }
//        }
//
//        //        } else {
//        //            self.view.makeToast("Please check internet connection", duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil)
//        //        }
//
//    }
//
//    func getFBUserData(){
//
//        if((FBSDKAccessToken.current()) != nil){
//
//            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
//                if (error == nil){
//
//                    let firstname = (result as AnyObject).value(forKey: "first_name") as! String
//                    let lastname = (result as AnyObject).value(forKey: "last_name") as! String
//                    var email = ""
//
//                    let profile_id = (result as AnyObject).value(forKey: "id") as! String
//
//                    if let _ = (result as AnyObject).value(forKey: "email"){
//
//                        email = (result as AnyObject).value(forKey: "email") as! String
//                    }
//                    let fbuser = ["first_name":firstname,"last_name":lastname,"email":email,"action" : "SocialAuth","provider" : "Facebook","fcm_token" : "SaMpLeToKeN123456","device_id" : UIDevice.current.identifierForVendor?.uuidString ?? "","device_type" : "iOS","profile_id" : profile_id]
//
//
//                    print(fbuser)
//
//                    //                    self.doLOGIn(data: fbuser)
//                }
//            })
//        }
//    }
//
//
//    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
//
//        if(result == nil) {
//            return
//        }
//
//        let accessToken = FBSDKAccessToken.current()
//
//        if(accessToken != nil) {
//
//            print(accessToken?.tokenString ?? "")
//
//        }else{
//
//            return
//        }
//
//        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,email,name,first_name,last_name"], tokenString: accessToken!.tokenString, version: nil, httpMethod: "GET")
//        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
//            if ((error) != nil) {
//                //                self.view.makeToast("Some Error Occured", duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
//
//            } else{
//
//                let firstname = (result as AnyObject).value(forKey: "first_name") as! String
//                let lastname = (result as AnyObject).value(forKey: "last_name") as! String
//                var email=""
//                let profile_id = (result as AnyObject).value(forKey: "id") as! String
//                if let _ = (result as AnyObject).value(forKey: "email") {
//
//                    email = (result as AnyObject).value(forKey: "email") as! String
//                }
//
//                let fbuser = ["first_name":firstname,"last_name":lastname,"email":email,"action" : "SocialAuth","provider" : "Facebook","fcm_token" : "SaMpLeToKeN123456","device_id" : UIDevice.current.identifierForVendor?.uuidString ?? "","device_type" : "iOS","profile_id" : profile_id]
//
//                print(fbuser)
//                //                self.doLOGIn(data: fbuser)
//
//            }
//        })
//
//    }
//
//    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
//
//    }
//}




extension ViewController : ButtonViewDelegate{
    func pressedBlueButton() {
      self.performSegue(withIdentifier: "VCToLocationVC", sender: self)
    }
    
    func pressedRedButton() {
       
    }
    
    
}
