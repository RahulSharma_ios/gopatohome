//
//  MainLoginVC.swift
//  GoPatoHome
//
//  Created by apple on 02/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

class MainLoginVC: UIViewController {

    private var commom_Variables_and_Methods : Common_Variables_and_Methods!
    @IBOutlet weak var image_Left: UIView!
    @IBOutlet weak var image_Right: UIView!
    
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var btn_Allow: UIButton!
    
    @IBOutlet weak var btnLbl: UILabel!
    
    @IBOutlet weak var hiddenView: UIView!
    
    @IBOutlet weak var rightHook: NSLayoutConstraint!
    @IBOutlet weak var leftHook: NSLayoutConstraint!
    @IBOutlet weak var buttonView: UIView!
    
    
    @IBOutlet weak var view_Email: UIView!
    
    @IBOutlet weak var view_Password: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tf_Email: UITextField!
    
    @IBOutlet weak var tf_Password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.commom_Variables_and_Methods = Common_Variables_and_Methods.getInstance()
        self.setupView()
    }
    
    @IBAction func pressedHiddenButton(_ sender: UIButton) {
        
    }
    
    private  func setupView(){
        
        setKeyBoardObserver()
        tf_Email.delegate = self
        tf_Password.delegate = self
        view_Email.layer.cornerRadius = (view_Email.frame.height / 2)
        view_Password.layer.cornerRadius = ( view_Password.frame.height / 2 )
        
        btn_Allow.isHidden = true
        //btn_Cancel.isHidden = true
      
        
        
        commom_Variables_and_Methods.getRoundedCorner(view: buttonView , shadowEffect: true)
        commom_Variables_and_Methods.getRoundedCorner(view: btn_Allow , shadowEffect: true)
        commom_Variables_and_Methods.getRoundedCorner(view: btn_Cancel , shadowEffect: true)
//
        self.btn_Cancel.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner ]
        self.btn_Allow.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner ]

        btn_Allow.addTarget(self, action: #selector(pressed_AllowBtn), for: .touchUpInside)
        btn_Cancel.addTarget(self, action: #selector(pressed_CancelBtn), for: .touchUpInside)

      pressed_AllowBtn()
    }

    
    override func keyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo
        let keyboardSize =  info?["UIKeyboardFrameEndUserInfoKey"] as! CGRect

        self.scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: keyboardSize.height + 50 , right: 0)
    }
    
    
    
    override func keyboardWillHide(notification: NSNotification) {
        if self.scrollView.frame.origin.y != 0 {
            self.scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0  , right: 0)
        }
    }
    
    @objc func pressed_AllowBtn(){
        
        
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.rightHook.constant = self.commom_Variables_and_Methods.defualthookValue
            self.leftHook.constant = self.commom_Variables_and_Methods.hookValue
            self.image_Right.isHidden = false
            self.image_Left.isHidden = true
            self.btn_Cancel.isHidden = false
            self.btn_Allow.isHidden = true
            self.buttonView.backgroundColor = .getUIColor(color: "4C82EE")
            self.btnLbl.text = "Log In"
            self.hiddenView.isHidden = false
        })
        
    }
    
    
    @objc func pressed_CancelBtn(){
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.leftHook.constant = self.commom_Variables_and_Methods.defualthookValue
            self.rightHook.constant = self.commom_Variables_and_Methods.hookValue
            self.image_Right.isHidden = true
            self.image_Left.isHidden = false
            self.btn_Allow.isHidden = false
            self.btn_Cancel.isHidden = true
            self.hiddenView.isHidden = true
            self.buttonView.backgroundColor = .getUIColor(color: "F95C5C")
            self.btnLbl.text = "Cancel"
        })
    }
    
}

extension MainLoginVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}
