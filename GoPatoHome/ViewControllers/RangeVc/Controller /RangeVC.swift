//
//  RangeVC.swift
//  GoPatoHome
//
//  Created by 3Embed on 08/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

class RangeVC: UIViewController {

    
    var locationName = ""
    private var commom_Variables_and_Methods : Common_Variables_and_Methods!
    @IBOutlet weak var locationHiddenButton: UIButton!
    @IBOutlet weak var tf_Email: UITextField!
   
    @IBOutlet weak var lbl_LocationName: UILabel!
    
    @IBOutlet weak var pin_Image: UIImageView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var lbl_FirstMsg: UILabel!
    
    @IBOutlet weak var lbl_Second: UILabel!
    
    @IBOutlet weak var locationView: UIView!
    
    
    @IBOutlet weak var image_Left: UIView!
    @IBOutlet weak var image_Right: UIView!
    
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var btn_Allow: UIButton!
    
    @IBOutlet weak var btnLbl: UILabel!
    
    
    @IBOutlet weak var rightHook: NSLayoutConstraint!
    @IBOutlet weak var leftHook: NSLayoutConstraint!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commom_Variables_and_Methods =  Common_Variables_and_Methods.getInstance()
        tf_Email.delegate = self
        self.setupView()
    }
    
    private func setupView(){
        self.image_Left.isHidden = true
            buttonView.layer.cornerRadius = buttonView.frame.height / 2
        
        
        commom_Variables_and_Methods.getShadwoView_10radius(view: locationView)
        
        if UIDevice.current.screenType == .iPhone5 {
            lbl_LocationName.font = UIFont.systemFont(ofSize: 13)
        }else{
              lbl_LocationName.font = UIFont.systemFont(ofSize: 13)
        }
        lbl_LocationName.text = locationName
        
//        locationView.layer.borderColor = UIColor.gray.cgColor
//        locationView.layer.borderWidth = 1.0
//        locationView.layer.cornerRadius = locationView.frame.height / 2
        
        
       commom_Variables_and_Methods.getRoundedCorner(view: buttonView , shadowEffect: true)
        commom_Variables_and_Methods.getRoundedCorner(view: btn_Allow , shadowEffect: true)
        commom_Variables_and_Methods.getRoundedCorner(view: btn_Cancel , shadowEffect: true)
        
        
        self.btn_Cancel.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner ]
        self.btn_Allow.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner ]
        
        btn_Allow.addTarget(self, action: #selector(pressed_AllowBtn), for: .touchUpInside)
        btn_Cancel.addTarget(self, action: #selector(pressed_CancelBtn), for: .touchUpInside)
        self.pressed_AllowBtn()
    }
    
    
    @objc func pressed_AllowBtn(){
        
        
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.rightHook.constant = self.commom_Variables_and_Methods.defualthookValue
            self.leftHook.constant = self.commom_Variables_and_Methods.hookValue
            self.image_Right.isHidden = false
            self.image_Left.isHidden = true
            self.btn_Cancel.isHidden = false
            self.btn_Allow.isHidden = true
            self.buttonView.backgroundColor = .getUIColor(color: "4C82EE")
            self.btnLbl.text = "Accept"
            self.locationView.isHidden = false
        })
        
    }
    
    
    @objc func pressed_CancelBtn(){
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.leftHook.constant = self.commom_Variables_and_Methods.defualthookValue
            self.rightHook.constant = self.commom_Variables_and_Methods.hookValue
            
            self.image_Right.isHidden = true
            self.image_Left.isHidden = false
            self.btn_Allow.isHidden = false
            self.btn_Cancel.isHidden = true
           
            self.buttonView.backgroundColor = .getUIColor(color: "F95C5C")
            self.btnLbl.text = "Cancel"
        })
    }

    @IBAction func pressedHiddenButton(_ sender: UIButton) {
        
        self.locationView.layer.borderColor  = UIColor.getUIColor(color: "4C82EE").cgColor
        pin_Image.isHidden = true
        lbl_LocationName.isHidden = true
        tf_Email.isHidden = false
        locationHiddenButton.isHidden = true
    }
    
}


extension RangeVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
        return true
    }
    
}
