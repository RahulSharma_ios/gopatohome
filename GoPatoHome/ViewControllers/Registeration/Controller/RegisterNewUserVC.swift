//
//  RegisterNewUserVC.swift
//  GoPatoHome
//
//  Created by 3Embed on 09/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit
import ActiveLabel
class RegisterNewUserVC: UIViewController {

    
    private let array = ["1","2","3", "4","5", "6","7","8" ,"9","10"]
    
    
    private var  commom_Variables_and_Methods  : Common_Variables_and_Methods!
  
    @IBOutlet weak var buttonViewBottomAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var buttonView: UIView!
  
    @IBOutlet weak var bottomView: ButtonView!
    
    @IBOutlet weak var image_Left: UIView!
    @IBOutlet weak var image_Right: UIView!
    
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var btn_Allow: UIButton!
    
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var btnLbl: UILabel!
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var rightHook: NSLayoutConstraint!
    @IBOutlet weak var leftHook: NSLayoutConstraint!
    
    @IBOutlet weak var containerViewTopAnchor: NSLayoutConstraint!
    @IBOutlet weak var nameView: UIView!
    
    @IBOutlet weak var phonenoView: UIView!
    
    @IBOutlet weak var emailView: UIView!
    
    @IBOutlet weak var btn_Female: UIButton!
    @IBOutlet weak var dateViewfull: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var lbl_TermsCondition: ActiveLabel!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var btn_Male: UIButton!
    
    @IBOutlet weak var horizontalSapratorView: UIView!
    
    
    
    //MARK:- textFields
    
    @IBOutlet weak var tf_Name: UITextField!
    
    @IBOutlet weak var tf_PhoneNumber: UITextField!
    
    @IBOutlet weak var tf_Email: UITextField!
    
    @IBOutlet weak var tf_Password: UITextField!
   
    @IBOutlet weak var dateButton: UIButton!
    
    private var tableView : UITableView!
    private var hiddenView : UIView!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commom_Variables_and_Methods = Common_Variables_and_Methods.getInstance()
        self.bottomView.delegate = self 
        self.setKeyBoardObserver()
        self.setupView()
        self.containerViewTopAnchor.constant = 100
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.containerViewAnimation()
    }
 
    private func containerViewAnimation(){

        UIView.animate(withDuration: 0.3) {
            
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.containerViewTopAnchor.constant = 0
            self.view.layoutIfNeeded()
            
        }) { (isDone) in
            if isDone {
                self.nameView.isHidden = false
                self.emailView.isHidden = false
                self.dateView.isHidden = false
                self.phonenoView.isHidden = false
                self.passwordView.isHidden = false
                self.lbl_TermsCondition.isHidden = false
                self.dateViewfull.isHidden = false
            }
        }
        
    }
    
    
    
    @IBAction func pressedDatePicker(_ sender: UIButton) {
        
       let customDatePicker =  CustomDatePicker(vc: self, sender: dateButton)
       customDatePicker.delegate = self
        
    }
    
    @IBAction func tapOnPhoneLabel(_ sender: UITapGestureRecognizer) {
        
        presentTableViewWithAnimation()
        
    }
    
    @IBAction func tapOnIamgeView(_ sender: Any) {
        presentTableViewWithAnimation()
        
    }
    
    private func presentTableViewWithAnimation(){
        print(lblCountryCode.frame , containerView.frame.origin.y , phonenoView.frame.origin.y)
        tableView =  UITableView(frame: CGRect(x: 30, y: containerView.frame.origin.y + phonenoView.frame.origin.y + 20, width: 90, height: self.view.frame.height / 2 ), style: .plain)
        
        
    
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
       
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false  
        hiddenView = UIView(frame: CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height))
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hiddenViewHide))
        
        tableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner ,.layerMaxXMaxYCorner , .layerMinXMaxYCorner]
        tableView.layer.cornerRadius = 18
        tableView.clipsToBounds = true
        
        
        hiddenView.addGestureRecognizer(tap)
        hiddenView.backgroundColor = .black
        hiddenView.layer.opacity = 0.5
        
        
        tableView.bringSubviewToFront(hiddenView)
        
        
        
        self.view.addSubview(hiddenView)
        self.view.addSubview(tableView)
      
        
//        UIView.animate(withDuration: 0.5, animations: {
//
//        })
    }
    
    @objc func hiddenViewHide(){
        tableView.isHidden = true
        hiddenView.isHidden = true
        hiddenView.removeFromSuperview()
        tableView.removeFromSuperview()
    }
    
    private func setupView(){
        
     horizontalSapratorView.backgroundColor = .getUIColor(color: "4C82EE")
        commom_Variables_and_Methods.setBorderAndcorners(view: nameView)
        commom_Variables_and_Methods.setBorderAndcorners(view: phonenoView)
        commom_Variables_and_Methods.setBorderAndcorners(view: emailView)
        commom_Variables_and_Methods.setBorderAndcorners(view: dateView)
        commom_Variables_and_Methods.setBorderAndcorners(view: passwordView)
        
        self.image_Left.isHidden = true
    
        btn_Male.addTarget(self, action: #selector(pressedMaleButton), for: .touchUpInside)
    
          btn_Female.addTarget(self, action: #selector(pressedFemaleButton), for: .touchUpInside)
        
        // MARK:- setup Active Label
        
        let customType = ActiveType.custom(pattern: "\\sTerms & Conditions\\b") //Regex that looks for "with"
        lbl_TermsCondition.enabledTypes = [ customType]
        lbl_TermsCondition.text = "By creating this account, you agree to our Terms & Conditions"
        lbl_TermsCondition.customColor[customType] = .getUIColor(color: "4C82EE")
        lbl_TermsCondition.customSelectedColor[customType] = UIColor.gray
        
        lbl_TermsCondition.handleCustomTap(for: customType) { element in
            
            self.performSegue(withIdentifier: "signUpToTerms", sender: self)
            
            print("Custom type tapped: \(element)")
        }
        
      //  containerView.layer.cornerRadius = 30
       // containerView.clipsToBounds = true
       // containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        
        commom_Variables_and_Methods.getRoundedCorner(view: buttonView , shadowEffect: true)
        commom_Variables_and_Methods.getRoundedCorner(view: btn_Allow , shadowEffect: true)
        commom_Variables_and_Methods.getRoundedCorner(view: btn_Cancel , shadowEffect: true)
        
//        btn_Allow.layer.cornerRadius = btn_Cancel.frame.height / 2
//        btn_Allow.layer.shadowColor = UIColor.gray.cgColor
//        btn_Allow.layer.shadowRadius = 2.0
//        btn_Allow.layer.shadowOpacity = 1.0
//        btn_Allow.layer.shadowOffset = CGSize(width: 0, height: 1)
//
//        btn_Cancel.layer.cornerRadius = btn_Cancel.frame.height / 2
//        btn_Cancel.layer.shadowColor = UIColor.gray.cgColor
//        btn_Cancel.layer.shadowRadius = 2.0
//        btn_Cancel.layer.shadowOpacity = 1.0
//        btn_Cancel.layer.shadowOffset = CGSize(width: 0, height: 1)
        
       btn_Female.layer.cornerRadius = btn_Female.frame.height / 2
        
       btn_Male.layer.cornerRadius = btn_Male.frame.height / 2
        
         btn_Male.layer.borderWidth = 1.0
         btn_Female.layer.borderWidth = 1.0
        
        btn_Male.layer.borderWidth = 1.0
        btn_Female.layer.borderWidth = 1.0
        btn_Male.borderColor = .getUIColor(color: "4C82EE")
        btn_Female.borderColor = .getUIColor(color: "4C82EE")
        
        
        tf_Name.delegate = self
        tf_Email.delegate = self
        tf_Password.delegate = self
        tf_PhoneNumber.delegate = self
        
        self.btn_Cancel.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner ]
        self.btn_Allow.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner ]
        
        btn_Allow.addTarget(self, action: #selector(pressed_AllowBtn), for: .touchUpInside)
        btn_Cancel.addTarget(self, action: #selector(pressed_CancelBtn), for: .touchUpInside)
        
        
        //self.pressed_AllowBtn()
    }
    
    
    override func keyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo
        let keyboardSize =  info?["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        
        
        self.scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: keyboardSize.height  , right: 0)
        buttonViewBottomAnchor.constant = keyboardSize.height
//        UIView.animate(withDuration: 0.6) {
////            self.stackViewBottomAncher.constant  = keyboardSize.height
//
//
//            self.view.layoutIfNeeded()
//        }
        
        //
        //self.scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: keyboardSize.height + 50 , right: 0)
    }
    
    override func keyboardWillHide(notification: NSNotification) {
        
        self.scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom:  0 , right: 0)
        
        buttonViewBottomAnchor.constant = commom_Variables_and_Methods.buttonViewBottomAnchor
//        if self.stackViewBottomAncher.constant != 0.0 {
//
//            self.updateLayoutWithViews(boolValue: true, anchorValue: 25.0)
//            self.stackViewBottomAncher.constant = 0.0
//            currentLocationView.isHidden = false
//        }
    }
    
    @objc func pressed_AllowBtn(){
        
        
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.rightHook.constant = self.commom_Variables_and_Methods.defualthookValue
            self.leftHook.constant = self.commom_Variables_and_Methods.hookValue
            self.image_Right.isHidden = false
            self.image_Left.isHidden = true
            self.btn_Cancel.isHidden = false
            self.btn_Allow.isHidden = true
            self.buttonView.backgroundColor = .getUIColor(color: "4C82EE")
            self.btnLbl.text = "Sign Up"
            
        })
        
    }
    
    @objc func pressedMaleButton(){
        
    btn_Female.backgroundColor = .white
    btn_Female.setTitleColor(.black, for: .normal)
        
    btn_Male.backgroundColor = .getUIColor(color: "4C82EE")
        btn_Male.setTitleColor(.white, for: .normal)
    }
    
    
    @objc func pressedFemaleButton(){
        btn_Male.backgroundColor = .white
        btn_Male.setTitleColor(.black, for: .normal)
        
        btn_Female.backgroundColor = .getUIColor(color: "4C82EE")
        btn_Female.setTitleColor(.white, for: .normal)
        
    }
    
    @objc func pressed_CancelBtn(){
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.leftHook.constant = self.commom_Variables_and_Methods.defualthookValue
            self.rightHook.constant = self.commom_Variables_and_Methods.hookValue
            
            self.image_Right.isHidden = true
            self.image_Left.isHidden = false
            self.btn_Allow.isHidden = false
            self.btn_Cancel.isHidden = true
            
            self.buttonView.backgroundColor = .getUIColor(color: "F95C5C")
            self.btnLbl.text = "Cancel"
        })
    }
    
    @IBAction func pressedHiddenButton(_ sender: UIButton) {
        
        
        
      
    }
    
}


extension RegisterNewUserVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
        return true
        
    }
    
}


extension  RegisterNewUserVC : CustomDatePickerDelegate{
    
    func updateDate(date: String) {
        dateButton.setTitle(date, for: .normal)
    }
}


extension RegisterNewUserVC : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = array[indexPath.row]
        cell.textLabel?.textAlignment = .center
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lblCountryCode.text = array[indexPath.row]
        tableView.isHidden = true
        hiddenView.isHidden = true
        hiddenView.removeFromSuperview()
        tableView.removeFromSuperview()
    }
    
}


extension RegisterNewUserVC : ButtonViewDelegate{
    func pressedBlueButton() {
      self.performSegue(withIdentifier: "RegisterVCToHomeVc", sender: self)
    }
    
    func pressedRedButton() {
      
    }
    
    
}
