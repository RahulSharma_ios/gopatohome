//
//  SignInLocationVC.swift
//  GoPatoHome
//
//  Created by apple on 02/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit
import CoreLocation
class SignInLocationVC: UIViewController {
    private var  commom_Variables_and_Methods  :  Common_Variables_and_Methods!
  
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var backGroundRightAncher: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_AlertMsg: UILabel!
    @IBOutlet weak var image_Left: UIView!
    @IBOutlet weak var image_Right: UIView!
    
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var btn_Allow: UIButton!
    
    @IBOutlet weak var btnLbl: UILabel!
    
    @IBOutlet weak var hiddenView: UIView!
    
    @IBOutlet weak var rightHook: NSLayoutConstraint!
    @IBOutlet weak var leftHook: NSLayoutConstraint!
    
    @IBOutlet weak var alertView: UIView!
   
    @IBOutlet weak var alertViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var imageViewHome: UIImageView!
    @IBOutlet weak var imageViewGoPaTo: UIImageView!
    @IBOutlet weak var buttonView: UIView!
    
    
    @IBOutlet weak var backgroundImageViewBottomAncchor: NSLayoutConstraint!
    @IBOutlet weak var viewLocation: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commom_Variables_and_Methods =  Common_Variables_and_Methods.getInstance()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideCustomAlertView))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        alertView.isHidden = true
        alertView.layer.cornerRadius = 30
        alertView.clipsToBounds = true
        alertView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        commom_Variables_and_Methods.getShadwoView_10radius(view: viewLocation)
        
       
        setupView()
        
    }
    
    @objc func hideCustomAlertView(){
        
    }
    
    
    @IBAction func pressedAlertViewHiddenButton(_ sender: UIButton) {
        
        if btnLbl.text == "Allow"{
            checkLocationService()
        }else if btnLbl.text == "Cancel" {
            self.navigationController?.popViewController(animated: true)

        }
    }
    
    
    private func setupView(){
        buttonView.isHidden = true
        btn_Allow.isHidden = true
        btn_Cancel.isHidden = true
        
        
        commom_Variables_and_Methods.getRoundedCorner(view: buttonView , shadowEffect: true)
        commom_Variables_and_Methods.getRoundedCorner(view: btn_Allow , shadowEffect: true)
        commom_Variables_and_Methods.getRoundedCorner(view: btn_Cancel , shadowEffect: true)
        
        
        self.btn_Cancel.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner ]
        self.btn_Allow.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner ]
        
        btn_Allow.addTarget(self, action: #selector(pressed_AllowBtn), for: .touchUpInside)
        btn_Cancel.addTarget(self, action: #selector(pressed_CancelBtn), for: .touchUpInside)
        
        
        alertViewHeight.constant = 0.0
        
        UIView.animate(withDuration: 0.8, animations: {
            
            self.viewLocation.isHidden = false
            self.viewLocation.layer.opacity += 0.9
        }, completion: nil)
        
    }

    @IBAction func pressedHiddenButton(_ sender: UIButton) {
        
        
        if commom_Variables_and_Methods.getStringData_From_UserDefaults(key: commom_Variables_and_Methods.key_ALLOWED_LOCATION_ACCESS) == "Y"{
            
            self.performSegue(withIdentifier: "signInVCToLocationVC", sender: self)
            
        }else{
        
        
        UIView.animate(withDuration: 0.8, animations: {
          
                self.viewLocation.layer.opacity = 0.0
                self.viewLocation.isHidden = false
            
        }) { (true) in
            UIView.animate(withDuration: 0.8  , animations: {
                
                
                // MARK:- image animation 
                self.self.backGroundRightAncher.constant = 0.0
                self.backgroundImageViewBottomAncchor.constant = 0
                 self.view.layoutIfNeeded()
 
            }){(finished) in
                if finished{
             
                }
                
            }
            
            UIView.animate(withDuration: 1.0, delay: 0.2, options: [.transitionFlipFromBottom], animations: {
                self.alertView.isHidden = false
                self.alertViewHeight.constant = (self.view.frame.height / 2 ) + 50
                self.view.layoutIfNeeded()
                
            }) { (finished) in
                
                
                UIView.animate(withDuration: 0.6, delay: 0.2 ,animations: {
                    self.imageViewHome.layer.opacity -= 1.0
                    self.imageViewGoPaTo.layer.opacity -= 1.0
                    
                    self.imageViewHome.isHidden = true
                    self.imageViewGoPaTo.isHidden = true
                    self.view.layoutIfNeeded()
                }) {(finished) in
                    if finished {
                         self.pressed_AllowBtn()
                        self.buttonView.isHidden = false
//                        self.lbl_AlertMsg.isHidden = false
                    }
                }
            }
        }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signInVCToLocationVC" {
            if let vc = segue.destination as? LocationSearchingVC {
                vc.isAllowed = true
                  vc.isAllowed = true
            }
        }
       
    }
   
    
    func setupLocationManager(){
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
    }
    
    
    func checkLocationService(){
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
            locationManager.startUpdatingLocation()
        }else{
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    func checkLocationAuthorization() {
        
        switch CLLocationManager.authorizationStatus() {
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
            
        case .denied :
            
            break
            
        case .restricted :
            self.locationManager.requestWhenInUseAuthorization()
            break
            
        case .authorizedAlways:

             self.performSegue(withIdentifier: "signInVCToLocationVC", sender: self)
            break
            
        case .authorizedWhenInUse:
    
             self.performSegue(withIdentifier: "signInVCToLocationVC", sender: self)
            break
            
        default:
            break
        }
        
    }
    
    
    @objc func pressed_AllowBtn(){
        
        
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.rightHook.constant = self.commom_Variables_and_Methods.defualthookValue
            self.leftHook.constant = self.commom_Variables_and_Methods.hookValue
            self.image_Right.isHidden = false
            self.image_Left.isHidden = true
            self.btn_Cancel.isHidden = false
            self.btn_Allow.isHidden = true
            self.buttonView.backgroundColor = .getUIColor(color: "4C82EE")
            self.btnLbl.text = "Allow"
            self.hiddenView.isHidden = false
        })
        
    }
    
    
    @objc func pressed_CancelBtn(){
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.leftHook.constant = self.commom_Variables_and_Methods.defualthookValue
            self.rightHook.constant = self.commom_Variables_and_Methods.hookValue
            self.image_Right.isHidden = true
            self.image_Left.isHidden = false
            self.btn_Allow.isHidden = false
            self.btn_Cancel.isHidden = true
            self.hiddenView.isHidden = true
            self.buttonView.backgroundColor = .getUIColor(color: "F95C5C")
            self.btnLbl.text = "Cancel"
        })
    }
    
}



extension SignInLocationVC : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first{
            
//            DispatchQueue.main.async {
//                let position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude )
//
//                let marker = GMSMarker(position: position)
//                self.myMapView.camera = GMSCameraPosition(target: position, zoom: 15, bearing: 0, viewingAngle: 0)
//                marker.title = "myLocation"
//                marker.map = self.myMapView
//            }
            
        }
        
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if (status == CLAuthorizationStatus.authorizedWhenInUse || status == CLAuthorizationStatus.authorizedAlways ) {
            
            commom_Variables_and_Methods.setStringData_To_UserDefaults(stringValue: "Y", key: commom_Variables_and_Methods.key_ALLOWED_LOCATION_ACCESS)
             self.performSegue(withIdentifier: "signInVCToLocationVC", sender: self)
            
           // myMapView.isMyLocationEnabled = true
        }
        
    }
}


//
//extension SignInLocationVC : FBSDKLoginButtonDelegate{
//    
//    
//    
//    
//    
//    func facebookLogIn() {
//        
//        
//        
//        //        if isInternetAvailable() {
//        
//        
//        
//        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
//        
//        
//        
//        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
//            
//            
//            
//            if (error == nil){
//                
//                
//                
//                let fbloginresult : FBSDKLoginManagerLoginResult = result!
//                
//                if fbloginresult.grantedPermissions != nil{
//                    
//                    
//                    
//                    if(fbloginresult.grantedPermissions.contains("email")){
//                        
//                        
//                        
//                        self.getFBUserData()
//                        
//                    }
//                    
//                }
//                
//            }
//            
//        }
//        
//        
//        
//        //        } else {
//        
//        //            self.view.makeToast("Please check internet connection", duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil)
//        
//        //        }
//        
//        
//        
//    }
//    
//    
//    
//    func getFBUserData(){
//        
//        
//        
//        if((FBSDKAccessToken.current()) != nil){
//            
//            
//            
//            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
//                
//                if (error == nil){
//                    
//                    
//                    
//                    let firstname = (result as AnyObject).value(forKey: "first_name") as! String
//                    
//                    let lastname = (result as AnyObject).value(forKey: "last_name") as! String
//                    
//                    var email = ""
//                    
//                    
//                    
//                    let profile_id = (result as AnyObject).value(forKey: "id") as! String
//                    
//                    
//                    
//                    if let _ = (result as AnyObject).value(forKey: "email"){
//                        
//                        
//                        
//                        email = (result as AnyObject).value(forKey: "email") as! String
//                        
//                    }
//                    
//                    let fbuser = ["first_name":firstname,"last_name":lastname,"email":email,"action" : "SocialAuth","provider" : "Facebook","fcm_token" : "SaMpLeToKeN123456","device_id" : UIDevice.current.identifierForVendor?.uuidString ?? "","device_type" : "iOS","profile_id" : profile_id]
//                    
//                    
//                    
//                    
//                    
//                    print(fbuser)
//                    
//                    
//                    
//                    //                    self.doLOGIn(data: fbuser)
//                    
//                }
//                
//            })
//            
//        }
//        
//    }
//    
//    
//    
//    
//    
//    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
//        
//        
//        
//        if(result == nil) {
//            
//            return
//            
//        }
//        
//        
//        
//        let accessToken = FBSDKAccessToken.current()
//        
//        
//        
//        if(accessToken != nil) {
//            
//            
//            
//            print(accessToken?.tokenString ?? "")
//            
//            
//            
//        }else{
//            
//            
//            
//            return
//            
//        }
//        
//        
//        
//        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,email,name,first_name,last_name"], tokenString: accessToken!.tokenString, version: nil, httpMethod: "GET")
//        
//        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
//            
//            if ((error) != nil) {
//                
//                //                self.view.makeToast("Some Error Occured", duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
//                
//                
//                
//            } else{
//                
//                
//                
//                let firstname = (result as AnyObject).value(forKey: "first_name") as! String
//                
//                let lastname = (result as AnyObject).value(forKey: "last_name") as! String
//                
//                var email=""
//                
//                let profile_id = (result as AnyObject).value(forKey: "id") as! String
//                
//                if let _ = (result as AnyObject).value(forKey: "email") {
//                    
//                    
//                    
//                    email = (result as AnyObject).value(forKey: "email") as! String
//                    
//                }
//                
//                
//                
//                let fbuser = ["first_name":firstname,"last_name":lastname,"email":email,"action" : "SocialAuth","provider" : "Facebook","fcm_token" : "SaMpLeToKeN123456","device_id" : UIDevice.current.identifierForVendor?.uuidString ?? "","device_type" : "iOS","profile_id" : profile_id]
//                
//                
//                
//                print(fbuser)
//                
//                //                self.doLOGIn(data: fbuser)
//                
//                
//                
//            }
//            
//        })
//        
//        
//        
//    }
//    
//    
//    
//    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
//        
//        
//        
//    }
//    
//}
//
//
//
//
//
//extension SignInLocationVC {
//    
//    
//    
//    
//    
//    private func twitterLogin(){
//        
//        
//        
//        TWTRTwitter.sharedInstance().logIn(with: self, completion: { (session, error) in
//            
//            
//            
//            if (session != nil) {
//                
//                if let sess = session {
//                    
//                    print("signed in as \(sess.userName)");
//                    
//                    let twitterClient = TWTRAPIClient(userID: session?.userID)
//                    
//                    twitterClient.loadUser(withID: (session?.userID)!, completion: { (user, error) in
//                        
//                        
//                        
//                        if (error == nil){
//                            
//                            print(user!.profileImageURL)
//                            
//                            let dataArr = ["Text":"Twitter","name":user!.name,"image": user!.profileImageURL, "screen": user!.screenName] as [String : AnyObject]
//                            
//                        }
//                        
//                    })
//                    
//                }
//                
//            }
//            
//        })
//        
//    }
//    
//    
//    
//}
