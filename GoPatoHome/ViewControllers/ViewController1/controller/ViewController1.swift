//
//  ViewController1.swift
//  GoPatoHome
//
//  Created by 3Embed on 12/04/19.
//  Copyright © 2019 GoPato. All rights reserved.
//

import UIKit

class ViewController1: UIViewController {

   
    @IBOutlet weak var grayView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.view.backgroundColor = . gray
        // Do any additional setup after loading the view.
    }
    

   
    @IBAction func pressedActionButton(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.1, animations: {
            self.grayView.transform = CGAffineTransform(translationX: -160, y: 0)
        }, completion: nil)
    }
    
}
